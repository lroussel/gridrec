/*

 *   Copyright (C) 2022 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *       Author : Léon Roussel
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 **/

#ifndef _PSWF_GRIDDING_H
#define _PSWF_GRIDDING_H

#include <complex>
#include <pybind11/complex.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

/**
 * Compute the result matrices of the convolution between the
 * sinogram projected on the polar grid and the filter of convolution
 * w_table (PSWF)
 *
 * @param filtered_sinogram sinogram filtered (ramlak, hamming, ...)
 * @param U_table x coordinate in the polar projection
 * @param V_table y coordinate in the polar projection
 * @param w_table filter of convolution (PSWF)
 * @param c_f_len half length of the filter
 * @param TBLSPCG linear coefficient to access w_table
 *
 * @return convolution of filtered_sinogram & w_table
 * */
py::array_t<std::complex<double>> pswf_convolution_gridding(
    py::array_t<std::complex<double>> filtered_sinogram,
    py::array_t<double> U_table,
    py::array_t<double> V_table,
    py::array_t<double> w_table,
    double c_f_len,
    double TBLSPCG,
    int add_dc);

/**
 * Compute the result matrices of the convolution between the
 * sinogram projected on the polar grid and the filter of convolution
 * w_table (PSWF)
 *
 * @param filtered_sinogram sinogram filtered (ramlak, hamming, ...)
 * @param U_table x coordinate in the polar projection
 * @param V_table y coordinate in the polar projection
 * @param w_table filter of convolution (PSWF)
 * @param c_f_len half length of the filter
 * @param TBLSPCG coefficient to link the length of the filter
 * and w_table
 *
 * @return convolution of filtered_sinogram & w_table
 * */
py::array_t<std::complex<float>> pswf_convolution_gridding_float(
    py::array_t<std::complex<float>> filtered_sinogram,
    py::array_t<float> U_table,
    py::array_t<float> V_table,
    py::array_t<float> w_table,
    float c_f_len,
    float TBLSPCG,
    int add_dc);

#endif
