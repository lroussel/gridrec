/*

 *   Copyright (C) 2022 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *       Author : Léon Roussel
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 **/

#include <complex>
#include <pybind11/complex.h>
#include <pybind11/numpy.h>
#include <math.h>
#include <iostream>

namespace py = pybind11;

py::array_t<std::complex<double>> pswf_convolution_gridding(
    py::array_t<std::complex<double>> filtered_sinogram,
    py::array_t<double> U_table,
    py::array_t<double> V_table,
    py::array_t<double> w_table,
    double c_f_len,
    double TBLSPCG,
    int add_dc)
{
    // --------------------  INIT  ------------------------
    // Get buffers info :
    py::buffer_info filt_buff = filtered_sinogram.request(),
                    U_buff = U_table.request(),
                    V_buff = V_table.request(),
                    w_buff = w_table.request();

    // Get the start of each array :
    std::complex<double> *filt_ptr = (std::complex<double> *)filt_buff.ptr;
    double *U_ptr = (double *)U_buff.ptr,
           *V_ptr = (double *)V_buff.ptr,
           *w_ptr = (double *)w_buff.ptr;

    // Extract sinogram shape :
    int n_angles = filt_buff.shape[0];
    int ext_width = filt_buff.shape[1];

    // Allocate resulting matrix H :
    py::array_t<std::complex<double>> H = py::array_t<std::complex<double>>(ext_width * ext_width);
    py::buffer_info H_buff = H.request();
    std::complex<double> *H_ptr = (std::complex<double> *)H_buff.ptr;

    // Init matrice H with 0 :
    for (int x = 0; x < ext_width; x++)
    {
        for (int y = 0; y < ext_width; y++)
        {
            H_ptr[x * ext_width + y] = 0;
        }
    }

    // ----------------  COMPUTATION  --------------------
    int iul, iuh, ivl, ivh;
    double U, V;
    int iu, iv, x, y;
    int start_j = 1;
    if (add_dc)
    {
        start_j = 0;
    }

    // Fill H :
    for (int p = 0; p < n_angles; p++)
    {
        if (p % 400 == 0)
        { // logging
            printf("      %i%\n", (int)round((float)p / n_angles * 100));
        }

        // If add_dc == true -> add dc frequency <=> j=0 but only for
        // data1 (data2 is usually the symetric but it is the symetric
        // for j=0 is the highest frequency here, so we do not care)
        for (int j = start_j; j < ext_width / 2; j++)
        {
            std::complex<double> data1 = filt_ptr[p * ext_width + j];
            std::complex<double> data2 = 0;
            if (j != 0)
            {
                data2 = filt_ptr[p * ext_width + ext_width - j];
            }

            U = U_ptr[p * (ext_width / 2) + j];
            V = V_ptr[p * (ext_width / 2) + j];

            // setting border of the convolution :
            iul = ceil(U - c_f_len);
            iuh = floor(U + c_f_len);
            ivl = ceil(V - c_f_len);
            ivh = floor(V + c_f_len);
            if (iul < 1)
                iul = 1;
            if (iuh >= ext_width)
                iuh = ext_width - 1;
            if (ivl < 1)
                ivl = 1;
            if (ivh >= ext_width)
                ivh = ext_width - 1;

            // Convolve step :
            for (iu = iul; iu <= iuh; iu++)
            {
                x = (int)round(abs(U - iu) * TBLSPCG);
                for (iv = ivl; iv <= ivh; iv++)
                {
                    y = (int)round(abs(V - iv) * TBLSPCG);
                    const double convolv = w_ptr[x] * w_ptr[y];

                    H_ptr[iu * ext_width + iv] += convolv * data1;
                    if (j != 0)
                    {
                        H_ptr[(ext_width - iu) * ext_width + ext_width - iv] += convolv * data2;
                    }
                }
            }
        }
    }

    H.resize({ext_width, ext_width}); // rectangular numpy array
    return H;
}

py::array_t<std::complex<float>> pswf_convolution_gridding_float(
    py::array_t<std::complex<float>> filtered_sinogram,
    py::array_t<float> U_table,
    py::array_t<float> V_table,
    py::array_t<float> w_table,
    float c_f_len,
    float TBLSPCG,
    int add_dc)
{
    // --------------------  INIT  ------------------------
    // Get buffers info :
    py::buffer_info filt_buff = filtered_sinogram.request(),
                    U_buff = U_table.request(),
                    V_buff = V_table.request(),
                    w_buff = w_table.request();

    // Get the start of each array :
    std::complex<float> *filt_ptr = (std::complex<float> *)filt_buff.ptr;
    float *U_ptr = (float *)U_buff.ptr,
          *V_ptr = (float *)V_buff.ptr,
          *w_ptr = (float *)w_buff.ptr;

    // Extract sinogram shape :
    int n_angles = filt_buff.shape[0];
    int ext_width = filt_buff.shape[1];

    // Allocate resulting matrix H :
    py::array_t<std::complex<float>> H = py::array_t<std::complex<float>>(ext_width * ext_width);
    py::buffer_info H_buff = H.request();
    std::complex<float> *H_ptr = (std::complex<float> *)H_buff.ptr;

    // Init matrice H with 0 :
    for (int x = 0; x < ext_width; x++)
    {
        for (int y = 0; y < ext_width; y++)
        {
            H_ptr[x * ext_width + y] = 0;
        }
    }

    // ----------------  COMPUTATION  --------------------
    int iul, iuh, ivl, ivh;
    float U, V;
    int iu, iv, x, y;
    int start_j = 1;
    if (add_dc)
    {
        start_j = 0;
    }

    // Fill H :
    for (int p = 0; p < n_angles; p++)
    {
        if (p % 400 == 0)
        {
            printf("      %i%\n", (int)round((float)p / n_angles * 100));
        }

        for (int j = start_j; j < ext_width / 2; j++)
        {
            std::complex<float> data1 = filt_ptr[p * ext_width + j];
            std::complex<float> data2 = 0;
            if (j != 0)
            {
                data2 = filt_ptr[p * ext_width + ext_width - j];
            }

            U = U_ptr[p * (ext_width / 2) + j];
            V = V_ptr[p * (ext_width / 2) + j];

            // setting border of the convolution :
            iul = ceil(U - c_f_len);
            iuh = floor(U + c_f_len);
            ivl = ceil(V - c_f_len);
            ivh = floor(V + c_f_len);
            if (iul < 1)
                iul = 1;
            if (iuh >= ext_width)
                iuh = ext_width - 1;
            if (ivl < 1)
                ivl = 1;
            if (ivh >= ext_width)
                ivh = ext_width - 1;

            // Convolve step :
            for (iu = iul; iu <= iuh; iu++)
            {
                x = (int)round(abs(U - iu) * TBLSPCG);
                for (iv = ivl; iv <= ivh; iv++)
                {
                    y = (int)round(abs(V - iv) * TBLSPCG);
                    const float convolv = w_ptr[x] * w_ptr[y];

                    H_ptr[iu * ext_width + iv] += convolv * data1;
                    if (j != 0)
                    {
                        H_ptr[(ext_width - iu) * ext_width + ext_width - iv] += convolv * data2;
                    }
                }
            }
        }
    }

    H.resize({ext_width, ext_width}); // rectangular numpy array
    return H;
}