import numpy as np
import pytest
from gridrec.reconstruction import GridrecReconstructor


def disabled_test_check_sino():
    r = GridrecReconstructor((2, 4))
    r._check_sino(np.array([[1, 2, 3, 4], [5, 6, 7, 8]]))

    r = GridrecReconstructor((3, 5))
    r._check_sino(np.array([[1, 2, 3, 4, 5], [1, 2, 3, 4, 5], [5, 6, 7, 8, 6]]))

    with pytest.raises(ValueError):
        r._check_sino(np.array([[1, 2, 3, 4, 5], [1, 2, 3, 4, 5]]))


def disabled_test_padded_sinogram():
    r = GridrecReconstructor((2, 6))  # extended width is 8 = 2^4
    sino = np.array([[1, 2, 3, 4, 5, 6], [1, 2, 3, 4, 5, 6]])
    padded_sino = r._padded_sinogram(sino)
    p = np.array([[1, 2, 3, 4, 5, 6, 0, 0], [1, 2, 3, 4, 5, 6, 0, 0]])
    assert np.array_equal(padded_sino, p)

    r = GridrecReconstructor((2, 4))  # extended width is 4 = 2^2
    sino = np.array([[1, 2, 3, 4], [1, 2, 3, 4]])
    padded_sino = r._padded_sinogram(sino)
    assert np.array_equal(sino, padded_sino)


def test_pswf_convolution_gridding():
    r = GridrecReconstructor((8, 8), pad_ratio=1)
    sino = np.ones((8, 8))
    r.sino = r._padded_sinogram(sino)
    r.sino = r._fft_sinogram(r.sino)  # only DC freq
    H = r._pswf_convolution_gridding_non_opt()  # only 0s
    assert np.array_equal(H, np.zeros((r.extended_width, r.extended_width)))

    r = GridrecReconstructor(
        (8, 8), pad_ratio=1, options={"add_dc_freq": True}, filter_name="ramlak"
    )
    sino = np.ones((8, 8))
    r.sino = r._padded_sinogram(sino)
    r.sino = r._fft_sinogram(r.sino)
    H_dc = r._pswf_convolution_gridding_non_opt()
    assert not np.array_equal(
        H_dc, np.zeros((r.extended_width, r.extended_width), dtype=np.complex128)
    )

    r = GridrecReconstructor((8, 8), pad_ratio=1)
    sino = np.ones((8, 8))
    sino[5, 3] = 0
    sino[6, 2] = 0
    r.sino = r._padded_sinogram(sino)
    r.sino = r._fft_sinogram(r.sino)  # only DC freq
    H = r._pswf_convolution_gridding_non_opt()  # only 0s

    r = GridrecReconstructor(
        (8, 8), options={"add_dc_freq": True}, filter_name="ramlak"
    )
    r.sino = r._padded_sinogram(sino)
    r.sino = r._fft_sinogram(r.sino)
    H_dc = r._pswf_convolution_gridding_non_opt()
    assert not np.array_equal(H, H_dc)

    # optimized
    r = GridrecReconstructor(
        (8, 8), pad_ratio=1, options={"add_dc_freq": True}, filter_name="ramlak"
    )
    r.sino = r._padded_sinogram(sino)
    r.sino = r._fft_sinogram(r.sino)
    H_dc = r._pswf_convolution_gridding_opt()


def test_shift():
    r = GridrecReconstructor((2, 2))
    img = np.array([[1, 2], [3, 4]])
    assert np.array_equal(r._fft_shift(img), np.array([[4, 3], [2, 1]]))


def test_rot90():
    r = GridrecReconstructor((2, 2))
    img = np.array([[1, 2], [3, 4]])
    assert np.array_equal(r._rotation_90(img), np.array([[2, 4], [1, 3]]))

    img = np.array([[1, 2], [3, 4], [5, 6]])
    assert np.array_equal(r._rotation_90(img), np.array([[2, 4, 6], [1, 3, 5]]))


def test_fft():
    # tests on contants :
    r = GridrecReconstructor((4, 4))
    img = np.ones((8, 8))
    fft_img = r._ifft2_image(img)
    assert np.amax(np.abs(fft_img[:, 1:])) < 1e-7
    assert np.amax(np.abs(fft_img[1:, 0])) < 1e-7

    img = np.zeros((8, 8))
    img[0, 0] = 8 * 8
    # img = np.fft.fftshift(img)
    # print(img)
    fft_img = r._ifft2_image(img)
    print(fft_img)
    assert np.amax(np.abs(fft_img - np.ones((8, 8)))) < 1e-7
