# Compare the current result of gridrec.py with
# an older version that is used as a base implementation
import pytest
from gridrec.testutils import image_distance, get_data
from gridrec.reconstruction import (
    GridrecReconstructor,
    __have_c_gridrec__,
    __have_opencl_gridrec__,
)
import numpy as np


def test_center():
    # Tests with float 32 computation
    options = {"python_accelerate": True}
    r = GridrecReconstructor(
        (181, 640),
        pad_ratio=1.6,
        filter_name="ramlak_old",
        rot_center=295.4375,
        options=options,
    )
    sino = get_data("gridrec/dent_sino_181_640.npy")
    recon_py = r.reconstruct(sino.astype(np.float64))
    base_reconstrution = get_data("gridrec/recon_dent_c.npy").reshape(640, 640)

    dist_py = image_distance(base_reconstrution, recon_py, method="MAX")

    assert dist_py / np.amax(np.abs(recon_py)) < 1e-5  # low relative distance


def test_non_regression_brain():
    """
    Non regression test with a brain sinogram
    """
    # pad_ratio=1.33 yields padded_width = 1024
    # ramlak_old to keep the same filter as before
    r = GridrecReconstructor((500, 768), pad_ratio=1.33, filter_name="ramlak_old")
    sino = get_data("gridrec/brain_sino_500_768.npy")
    current_reconstruction = r.reconstruct(sino.astype(np.float64))

    # get the old reconstruction stored in a .dat file
    base_reconstrution = get_data("gridrec/recon_brain_py.npy")

    print("Shapes : ", base_reconstrution.shape, current_reconstruction.shape)
    print()
    print(base_reconstrution)
    print()
    print(current_reconstruction)

    dist = image_distance(base_reconstrution, current_reconstruction, method="MAX")

    print("Distance between images :", dist)

    # max|brain_image| ~= 280 => 0.001 is a 1e-5 precision

    assert dist < 0.001
    # if false --> new implementation is not the same as old implementation


def test_non_regression_accelerated():
    """
    Non regression test with a brain sinogram with accelerated reconstruction
    """
    options = {"python_accelerate": True}
    r = GridrecReconstructor(
        (500, 768), pad_ratio=1.33, filter_name="ramlak_old", options=options
    )
    sino = get_data("gridrec/brain_sino_500_768.npy")
    current_reconstruction = r.reconstruct(sino.astype(np.float64))

    # get the old reconstruction stored in a .dat file
    base_reconstrution = get_data("gridrec/recon_brain_py.npy")

    dist = image_distance(base_reconstrution, current_reconstruction, method="MAX")

    print("Distance between images :", dist)

    assert dist < 0.001


@pytest.mark.skipif(
    not (__have_c_gridrec__), reason="Need C/pybind11 backend for this test"
)
def test_non_regression_c():
    """
    Non regression test with a brain sinogram with c reconstruction
    """
    ### 1
    # add_dc = False by default
    r = GridrecReconstructor(
        (500, 768),
        pad_ratio=1.33,
        filter_name="ramlak_old",
    )
    sino = get_data("gridrec/brain_sino_500_768.npy")
    current_reconstruction = r.reconstruct_c(sino.astype(np.float64))
    base_reconstrution = get_data("gridrec/recon_brain_py.npy")

    dist = image_distance(base_reconstrution, current_reconstruction, method="MAX")

    print("Distance between images test 1:", dist)

    assert dist < 0.001

    ### 2
    options = {"add_dc_freq": True}
    r = GridrecReconstructor(
        (500, 768), pad_ratio=1.33, options=options, filter_name="ramlak_dc_old"
    )
    sino = get_data("gridrec/brain_sino_500_768.npy")
    current_reconstruction = r.reconstruct_c(sino.astype(np.float64))

    base_reconstrution = get_data("gridrec/recon_brain_dc_py.npy")

    dist = image_distance(base_reconstrution, current_reconstruction, method="MAX")

    print("Distance between images test 2:", dist)

    assert dist < 0.001


@pytest.mark.skipif(
    not (__have_opencl_gridrec__), reason="Need pyopencl backend for this test"
)
def test_non_regression_opencl():
    """
    Non regression test with a brain sinogram with OpenCL reconstruction
    Should have dtype=np.float32, OpenCL only implemented that way
    """
    ### 1
    # add_dc = False by default
    options = {"opencl_init": True}  # plan gridding (histogram + plan)
    r = GridrecReconstructor(
        (500, 768),
        pad_ratio=1.33,
        filter_name="ramlak_old",
        options=options,
        dtype=np.float32,
    )
    sino = get_data("gridrec/brain_sino_500_768.npy").astype(np.float32)
    current_reconstruction = r.reconstruct_opencl(sino)
    base_reconstrution = get_data("gridrec/recon_brain_py.npy")

    dist = image_distance(base_reconstrution, current_reconstruction, method="MAX")

    print("Distance between images test 1:", dist)

    # With double (C implementation could compute with double),
    # we have a 0.001 maximum gap, but with float, we have a 0.1 maximum gap
    # only, same for test_dtype()
    assert dist < 0.1  # 0.09

    ### 1bis
    options = {"opencl_init": True, "opencl_option": 3}  # atomic gridding
    r = GridrecReconstructor(
        (500, 768),
        pad_ratio=1.33,
        filter_name="ramlak_old",
        options=options,
        dtype=np.float32,
    )
    sino = get_data("gridrec/brain_sino_500_768.npy").astype(np.float32)
    current_reconstruction = r.reconstruct_opencl(sino)
    base_reconstrution = get_data("gridrec/recon_brain_py.npy")

    dist = image_distance(base_reconstrution, current_reconstruction, method="MAX")

    print("Distance between images test 1:", dist)

    # With double (C implementation could compute with double),
    # we have a 0.001 maximum gap, but with float, we have a 0.1 maximum gap
    # only, same for test_dtype()
    assert dist < 0.1  # 0.09

    ### 2
    options = {"add_dc_freq": True, "opencl_init": True}
    r = GridrecReconstructor(
        (500, 768),
        pad_ratio=1.33,
        options=options,
        filter_name="ramlak_dc_old",
        dtype=np.float32,
    )
    sino = get_data("gridrec/brain_sino_500_768.npy").astype(np.float32)
    current_reconstruction = r.reconstruct_opencl(sino)

    base_reconstrution = get_data("gridrec/recon_brain_dc_py.npy")

    dist = image_distance(base_reconstrution, current_reconstruction, method="MAX")

    print("Distance between images test 2:", dist)

    assert dist < 0.1


def test_fftw():
    """
    Non regression test with a brain sinogram with fftw
    """
    r = GridrecReconstructor(
        (500, 768), filter_name="ramlak_old", backend_fft="fftw", pad_ratio=1.33
    )
    sino = get_data("gridrec/brain_sino_500_768.npy")
    current_reconstruction = r.reconstruct_c(sino.astype(np.float64))

    # get the old reconstruction stored in a .dat file
    base_reconstrution = get_data("gridrec/recon_brain_py.npy")

    dist = image_distance(base_reconstrution, current_reconstruction, method="MAX")

    print("Distance between images :", dist)

    assert dist < 0.001


def test_dtype():
    # Tests with float 32 computation
    options = {"python_accelerate": True}
    r = GridrecReconstructor(
        (500, 768),
        pad_ratio=1.33,
        filter_name="ramlak_old",
        dtype=np.float32,
        options=options,
    )
    sino = get_data("gridrec/brain_sino_500_768.npy")
    recon_py = r.reconstruct(sino.astype(np.float32))
    recon_c = r.reconstruct_c(sino.astype(np.float32))
    base_reconstrution = get_data("gridrec/recon_brain_py.npy")

    dist_py = image_distance(base_reconstrution, recon_py, method="MAX")
    dist_c = image_distance(base_reconstrution, recon_c, method="MAX")

    assert dist_py < 0.1  # max values are 380 for brain
    assert dist_c < 0.1

    # TODO we still not have recon32py - recon32c == 0 ...
    # possibly, python could make calculation with double
    # same trouble with OpenCL implem
