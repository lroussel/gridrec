from gridrec.reconstruction import GridrecReconstructor
import numpy as np
import pytest


def test_simple_init():
    reconstructor_1 = GridrecReconstructor((4, 4))
    reconstructor_2 = GridrecReconstructor((2048, 2048))
    reconstructor_3 = GridrecReconstructor((500, 768))
    reconstructor_4 = GridrecReconstructor((5400, 3212))
    reconstructor_5 = GridrecReconstructor((6975, 1569))
    assert reconstructor_5.n_angles == 6975
    assert reconstructor_5.dwidth == 1569


def test_set_options():
    options = {
        "add_dc_freq": True,
    }
    r = GridrecReconstructor((4, 4), options=options, filter_name="ramlak")
    assert r.add_dc_freq

    options["add_dc_freq"] = False
    r = GridrecReconstructor((4, 4), options=options)
    assert not r.add_dc_freq
    # default
    r = GridrecReconstructor((4, 4))
    assert not r.add_dc_freq


def test_filtername():
    r = GridrecReconstructor((4, 4), filter_name="ramlak")
    r = GridrecReconstructor((4, 4), filter_name="hamming")


def test_init_constants():
    r = GridrecReconstructor((4, 4))
    assert r.C >= 0
    assert r.pol_approx_degree >= 0
    assert r.LAMBDA >= 0
    assert r.L // 2 == r.L2
    assert r.LTBL == 512


def test_init_geometry():
    r = GridrecReconstructor(
        (6, 4), slice_shape=(4, 4), rot_center=2, angles=np.array([0, 1, 2, 3, 4, 5])
    )
    assert r.dwidth == 4
    assert r.n_angles == 6
    assert r.rot_center == 2

    r = GridrecReconstructor(
        (11, 89),
        slice_shape=(4, 4),
        angles=np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    )
    assert r.dwidth == 89
    assert r.n_angles == 11
    assert r.rot_center == (89 - 1) / 2


def test_set_angles():
    r = GridrecReconstructor((6, 4), angles=np.array([0, 1, 2, 3, 4, 5]))
    assert r.n_angles == 6

    with pytest.raises(ValueError):
        r = GridrecReconstructor((2, 4), angles=np.array([0, 1, 2, 3, 4, 5]))

    # default
    r = GridrecReconstructor((4, 4))
    assert np.array_equal(
        r.angles, np.array([0, np.pi / 4, 2 * np.pi / 4, 3 * np.pi / 4])
    )


def test_set_slice_shape():
    r = GridrecReconstructor((4, 6), slice_shape=(10, 6))
    assert r.slice_shape == (10, 6)
    assert r.n_x, r.n_y == (6, 10)

    r = GridrecReconstructor((4, 6), slice_shape=10)
    assert r.slice_shape == (10, 10)
    assert r.n_x, r.n_y == (10, 10)

    # default
    r = GridrecReconstructor((4, 34))
    assert r.slice_shape == (34, 34)


def test_random_init_slice_shape():
    r1 = GridrecReconstructor((4, 4), slice_shape=(4, 4))

    r2 = GridrecReconstructor((2048, 2048), slice_shape=(1024, 1024))
    r3 = GridrecReconstructor((500, 768), slice_shape=(768, 768))
    r4 = GridrecReconstructor((5400, 3212), slice_shape=(6876, 5456))
    r5 = GridrecReconstructor((6975, 1569), slice_shape=(4568, 4698))
    assert r5.slice_shape == (4568, 4698)


def test_init_filter():
    with pytest.raises(ValueError):
        r = GridrecReconstructor((4, 4), filter_name="bad_name_for_filter")
