from gridrec.utils import filter_ramlak_dc_old, filter_ramlak_old
from silx.image.tomography import compute_fourier_filter
import numpy as np


def test_close_filter():
    l = 16
    ramlak_dc_old = filter_ramlak_dc_old(l)[: l // 2]
    ramlak_dc = compute_fourier_filter(l, "ramlak")[: l // 2]

    # relative difference is low :
    r_d = np.abs(ramlak_dc - ramlak_dc_old) / np.abs(ramlak_dc_old)
    for i in range(l // 2):
        assert r_d[i] < 1e-7
