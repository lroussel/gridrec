from gridrec.reconstruction import GridrecReconstructor
import numpy as np


def test_compute_trigo():
    r = GridrecReconstructor((4, 6))
    assert np.shape(r.cos) == (4,) and np.shape(r.sin) == (4,)


def test_legendre():
    r = GridrecReconstructor((4, 4))
    # Test with the sum of the 3 first coefficient of Legendre in x=0
    l = r._compute_sum_legendre(4, 0)
    l2 = (
        1 * r.LEGENDRE_COEFS[0]
        - 0.5 * r.LEGENDRE_COEFS[1]
        + 0.375 * r.LEGENDRE_COEFS[2]
    )
    print(l, l2)
    assert np.abs(l - l2) < 1e-5

    # x=0.5
    l = r._compute_sum_legendre(4, 0.5)
    l2 = (
        1 * r.LEGENDRE_COEFS[0]
        - 0.125 * r.LEGENDRE_COEFS[1]
        - 0.2890625 * r.LEGENDRE_COEFS[2]
    )
    print(l, l2)
    assert np.abs(l - l2) < 1e-5

    # x=-0.71
    l = r._compute_sum_legendre(4, -0.71)
    l2 = (
        1 * r.LEGENDRE_COEFS[0]
        + 0.25615 * r.LEGENDRE_COEFS[1]
        - 0.40361395625 * r.LEGENDRE_COEFS[2]
    )
    print(l, l2)
    assert np.abs(l - l2) < 1e-5


def test_compute_pswf_tables():
    # TODO find property to verify ?
    pass


def test_compute_main_filter():
    # TODO find property to verify ?
    pass
