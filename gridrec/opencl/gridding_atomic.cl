/*

 *   Copyright (C) 2022 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *       Author : Léon Roussel
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 **/

#include <pyopencl-complex.h>

// Atomic addition on float
inline void atomic_add_global_float(volatile global float *addr, float val) {
  union {
    uint u32;
    float f32;
  } next, expected, current;
  current.f32 = *addr;
  do {
    expected.f32 = current.f32;
    next.f32 = expected.f32 + val;
    current.u32 =
        atomic_cmpxchg((volatile global uint *)addr, expected.u32, next.u32);
  } while (current.u32 != expected.u32);
}

// Kernel executing the whole gridding, include atomic operation
__kernel void
gpu_gridding_atomic(__global cfloat_t *filtered_sino, int n_angles,
                    int ext_width, __global float *U_table,
                    __global float *V_table, __global float *w_table,
                    __global float *H_real, __global float *H_imag,
                    float c_f_len, float TBLSPCG, int add_dc) {
  int p = get_global_id(1);
  int j = get_global_id(0);

  if (!add_dc && j == 0) {
    return;
  }

  if (p * ext_width + j < ext_width * n_angles) {
    // Get sinogram datas
    cfloat_t data1 = filtered_sino[p * ext_width + j];
    cfloat_t data2 = cfloat_new(0., 0.);
    if (j != 0) {
      data2 = filtered_sino[p * ext_width + ext_width - j];
    }
    float U = U_table[p * (ext_width / 2) + j];
    float V = V_table[p * (ext_width / 2) +
                      j]; // cumputing in opencl ? or worse to import ?

    // Compute window of convolution
    int iul = ceil(U - c_f_len);
    int iuh = floor(U + c_f_len);
    int ivl = ceil(V - c_f_len);
    int ivh = floor(V + c_f_len);
    if (iul < 1)
      iul = 1;
    if (iuh >= ext_width)
      iuh = ext_width - 1;
    if (ivl < 1)
      ivl = 1;
    if (ivh >= ext_width)
      ivh = ext_width - 1;

    // 1 convolution step (H supposed to be 0):
    for (int iu = iul; iu <= iuh; iu++) {
      int x = (int)round(fabs(U - (float)iu) * TBLSPCG);
      for (int iv = ivl; iv <= ivh; iv++) {
        int y = (int)round(fabs(V - (float)iv) * TBLSPCG);
        const float convolv = w_table[x] * w_table[y];

        atomic_add_global_float(&H_real[iu * ext_width + iv],
                                cfloat_real(data1) * convolv);
        atomic_add_global_float(&H_imag[iu * ext_width + iv],
                                cfloat_imag(data1) * convolv);

        // Symmetric part
        if (j != 0) {
          atomic_add_global_float(
              &H_real[(ext_width - iu) * ext_width + ext_width - iv],
              cfloat_real(data2) * convolv);
          atomic_add_global_float(
              &H_imag[(ext_width - iu) * ext_width + ext_width - iv],
              cfloat_imag(data2) * convolv);
        }
      }
    }
  } else {
    printf("Warning : (p, j) = (%i, %i) out !\n", p, j);
  }
}