/*

 *   Copyright (C) 2022 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *       Author : Léon Roussel
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 **/

#include <pyopencl-complex.h>

#pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable

// Just compute sum of product between contribution_table and sinogram
// contribution_table store the indices of sinogram to access (p, j)
// contrib_filter_table store the filter coefficient (convolv_coeff)
__kernel void gpu_gridding(__global cfloat_t *filtered_sino,
                           __global int *contribution_table,
                           __global float *contrib_filter_table,
                           __global ulong *histogram, __global cfloat_t *H,
                           int ext_width, int add_dc) {
  int iu = get_global_id(1);
  int iv = get_global_id(0);

  int index = iu * ext_width + iv;

  for (ulong i = histogram[index]; i < histogram[index + 1]; i++) {
    int p = contribution_table[i * 2]; // '*2' because contribution_table of
                                       // shape (n, 2) in python
    int j = contribution_table[i * 2 + 1];

    cfloat_t data1 = filtered_sino[p * ext_width + j];

    const float filter_coeff = contrib_filter_table[i];
    H[index] = cfloat_add(
        H[index], cfloat_mulr(data1, filter_coeff)); // H[i] += data1 * coeff
  }
}


// Building histogram (still with int result)
__kernel void gpu_histogram(__global int *histogram, __global float *U_table,
                            __global float *V_table, int ext_width, int add_dc,
                            float c_f_len) {
  int i = get_global_id(0);
  int j = i % (ext_width / 2);
  // int p = get_global_id(1);
  // int j = get_global_id(0);
  // WHY it does not work in 2D ???
  // p in [0, n_angles]
  // j in [0, ext_w / 2]
  if (j == 0 && !add_dc) { 
        return;
  }

  float U = U_table[i];
  float V = V_table[i];
  int iul = (int)max(ceil(U - c_f_len), (float)1);
  int iuh = (int)min(floor(U + c_f_len), (float)(ext_width - 1));
  int ivl = (int)max(ceil(V - c_f_len), (float)1);
  int ivh = (int)min(floor(V + c_f_len), (float)(ext_width - 1));

  for (int iu = iul; iu < iuh + 1; iu++) {
    for (int iv = ivl; iv < ivh + 1; iv++) {
      atomic_inc(&histogram[iu * ext_width + iv]);
      if (j != 0) {
        atomic_inc(&histogram[(ext_width - iu) * ext_width + ext_width - iv]);
      }
    }
  }
}


// Compute the cumulated histogram (where the indexs are shifted by one)
// Not optimized, not appropriate to use one thread,
// but still so much faster than in Python... (better in C, but writing it in OpenCL
// avoid to get the C dependance)
__kernel void gpu_cumulated_histogram(__global int *histogram,__global int *cumul_histogram, int ext_width) {
  // Not used, not maintained
  // TODO overflow with int --> need long for the cumulated histogram
  int i = get_global_id(0);

  if(i >= ext_width * ext_width){
    return;
  }
  // lanch threads from 0 to ext_width^2

  int curr_sum = 0;
  for(int k = 0; k <= i ; k++){ // a lot of calculation for each threads...
    curr_sum += histogram[k];
  }

  cumul_histogram[i + 1] = curr_sum;
}

// Compute the cumulated histogram (where the indexs are shifted by one)
// Not optimized, but still so much faster than in Python... (reduction ?)
__kernel void gpu_cumulated_histogram_onethread(__global int *histogram,__global ulong *cumul_histogram, int ext_width) {
  int i = get_global_id(0);
  if(i != 0){
    return;
  }

  ulong curr_sum = 0;
  for(int k = 0; k < ext_width * ext_width ; k++){
    cumul_histogram[k] = curr_sum;
    curr_sum += (ulong) histogram[k]; // histogram[k] is an int
  }
  cumul_histogram[ext_width * ext_width] = curr_sum;
}

// Store all contribution grouped by (iu, iv) so that gpu_gridding could find
// easily all (U, V) contribution to a fix (iu, iv). We store (p, j) to get the
// sinogram index, and convolv_coeff which is the convolution filter coefficient
// in the gridding
__kernel void gpu_fill_contrib_table(
    __global ulong *cumul_hist, __global ulong *cumul_hist_copy,
    __global int *contrib_table, __global float *contrib_filter_table,
    __global float *U_table, __global float *V_table, __global float *w_table,
    int ext_width, int add_dc, float c_f_len, float TBLSPCG) {

  int i = get_global_id(0); // = p * ext_width + j
  int j = i % (ext_width / 2);
  int p = i / (ext_width / 2);

  if (j == 0 && !add_dc) {
    return;
  }

  float U = U_table[i];
  float V = V_table[i];
  int iul = (int)max(ceil(U - c_f_len), (float)1);
  int iuh = (int)min(floor(U + c_f_len), (float)(ext_width - 1));
  int ivl = (int)max(ceil(V - c_f_len), (float)1);
  int ivh = (int)min(floor(V + c_f_len), (float)(ext_width - 1));

  for (int iu = iul; iu < iuh + 1; iu++) {
    int x = (int)round(fabs(U - (float)iu) * TBLSPCG);
    for (int iv = ivl; iv < ivh + 1; iv++) {
      // (iu, iv) >=1 & < ext_width
      int y = (int)round(fabs(V - (float)iv) * TBLSPCG);
      float convolv_coeff = w_table[x] * w_table[y];
      int index = iu * ext_width + iv; // = iu * ext_width + iv
      int index2 = ext_width * (ext_width + 1) - index;
      // (symmetric) = (ext_width - iu) * ext_width + ext_width - iv

      // Top part :
      if (cumul_hist[index] != cumul_hist[index + 1]) {
        // wait to incremented before, to be sure that this index is unique:
        ulong contrib_index =
            atom_inc(&cumul_hist_copy[index]); // it returns old and store old
                                                 // + 1 at `index`
        contrib_table[contrib_index * 2] = p;    // unique access
        contrib_table[contrib_index * 2 + 1] = j;            // unique access
        contrib_filter_table[contrib_index] = convolv_coeff; // unique access
      }

      // Symmetric part :
      if (cumul_hist[index2] != cumul_hist[index2 + 1]) {
        if (j != 0) {
          ulong contrib_index = atom_inc(&cumul_hist_copy[index2]);
          contrib_table[contrib_index * 2] = p;                 // unique access
          contrib_table[contrib_index * 2 + 1] = ext_width - j; // unique access
          contrib_filter_table[contrib_index] = convolv_coeff;  // unique access
        }
      }
    }
  }
}
