# coding: utf-8
# ############################################################################
# Copyright (C) 2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# ############################################################################

__authors__ = ["Léon Roussel"]
__license__ = "MIT"
__date__ = "08/07/2022"

from copy import deepcopy
import logging
from math import ceil, floor
from matplotlib.pyplot import hist
import numpy as np
from silx.image.tomography import get_next_power, compute_fourier_filter
from silx.math import fft
from .utils import (
    export_wisdom,
    import_wisdom,
    get_wisdom_file,
    filter_ramlak_old,
    filter_ramlak_dc_old,
    legendre,
    is_in_rect,
    max_dist_to_rect,
    min_dist_to_rect,
    get_p,
)
import os

# import matplotlib.pyplot as plt

try:
    from gridrec import pswf_gridding_c

    __have_c_gridrec__ = True
except ImportError:
    __have_c_gridrec__ = False  # still possible to launch the python implementation

try:
    import pyopencl as cl

    __have_opencl_gridrec__ = True
except ImportError:
    __have_opencl_gridrec__ = (
        False  # still possible to launch the python implementation
    )

try:
    import pyfftw

    __have_pyfftw__ = True
except ImportError:
    __have_pyfftw__ = False  # stil possible to use default fft with numpy


class GridrecReconstructor:
    """
    Class to reconstruct an image using the gridding reconstruction method

    Parameters
    ------------
    sino_shape : (int, int)
        n_angles, dwidth
    slice_shape : int or (int, int)
        Size of the output reconstruction
    rot_center : int
        Position of the rotation center
            default is the middle
    angles : list of float
        List of acquisition angles
            default is equidistant angles in [0, PI[
    pad_ratio: float, optional
        Padding ratio of the sinogram width. The padded width is calculated as
        `next_power(int(width * pad_ratio))` where `next_power()` returns the next
        FFTW-friendly power. Value smaller than 1.5 may yield inter-period interference due to
        convolution actually being a circular convolution.
        default is 1.5
    filter_name : string
        Name of the chosen filter
            default is "ramlak"
    dtype : type (np.float32 or np.float64)
        Type used for FFT and gridding, return type of method reconstruct or reconstruct_c
        will always be np.float32
            default is np.float64
    backend_fft : string
        Which backend is used for FFT.
        Possible choice are numpy, fftw.
            default is numpy

        About fftw : Required pyfftw module
        Switch from numpy to pyfftw for the 2D-FFT.
        If activate, it will save the wisdom (computed parameter
        that are the best for fft with your computer) at
        /tmp/pyfftw and import this wisdom at each new call.
        pyfftw is not faster than numpy overall. It has 2 phases :
            * initialization of the plan (computation of paramaters
            to compute faster)
            * computation of the fft
        But if we store the data of the initialization and enable
        multi-threading, this become significantly faster on big
        images.

    options : dict
        see _set_options() method
    """

    def __init__(
        self,
        sino_shape,
        slice_shape=None,
        rot_center=None,
        angles=None,
        pad_ratio=1.5,
        filter_name="ramlak",
        dtype=np.float64,
        backend_fft="numpy",
        options=None,
    ):
        logging.basicConfig(level=logging.DEBUG, format="%(message)s")
        # debug, info, warning, ...

        logging.info("*** Python GRIDREC init ***")
        self._set_options(options)

        logging.debug("  [Init constants & geometry]")
        self._init_constants()
        self._init_type(dtype)
        self._init_geometry(sino_shape, pad_ratio, slice_shape, rot_center, angles)
        self._init_fft(backend_fft)

        logging.debug("  [Compute trigo & (U,V) & PSWF & filter]")
        self.cos = np.cos(self.angles, dtype=self.dtype)
        self.sin = np.sin(self.angles, dtype=self.dtype)
        self.U_table = (
            np.outer(self.cos, np.array(range(0, self.extended_width // 2)))
            + self.extended_width // 2
        ).astype(self.dtype)
        self.V_table = (
            np.outer(self.sin, np.array(range(0, self.extended_width // 2)))
            + self.extended_width // 2
        ).astype(self.dtype)

        # pswf_algrebra enable the computation of coefficients the the
        # construction of the PSWF. Otherwise, pre-calculated coefficient
        # are used, but they only works for C = 7.0
        if self.pswf_algrebra:
            self._compute_pswf_tables_algebra()
        else:
            self._compute_pswf_tables()

        # OpenCL plans initialization
        if self.opencl_init:
            if not __have_opencl_gridrec__:
                raise RuntimeError(
                    "reconstruct_opencl is not available, you may not have pyopencl installed"
                )
            logging.debug("  [OpenCL init]")
            self._init_plans_opencl()

        self._compute_main_filter(filter_name)

        logging.debug("*** Python GRIDREC end init ***")
        logging.debug("")

    # -----------------  Initialization methods : ------------------------
    def _set_options(self, options):
        """
        Initialize a few options of reconstruction.

        Parameters
        ------------
        options: dict
            The key is the name of the option

        OPTIONS :

            *) key: add_dc_freq, value: True or False
                Add the case j=0 (dc freq) during the gridding
                    default is False

            *) key: custom_normalization, value: np.float32
                Final normalisation of w_inverse (often include C)
                Default normalization is
                    with pswf_algebra not enabled :
                        np.sqrt(np.pi / 2 / self.C / self.LAMBDA) / 1.2
                    with pswf_algebra enabled :
                        1 / np.sqrt(1.2468 * self.C - 0.105)

                (sqrt because of tensor product w_inv[x]*w_inv[y])

            *) key: pswf_algebra, value: True or False
                Use the algebra way to compute PSWF instead of the
                legendre coefficient pre-computed for unique C=7.0
                    default is False

            *) key: C, value: positive float
                Setter of constant C
                If C is set to another value than 7.0, pswf_algebra should
                be set to True to compute new coefficients
                    default is 7

            *) key: continuous_convo, value: int
                TBLSPCG become continuous with C, there are no more floor
                continuous_convo = 0 : no change
                continuous_convo = 1 : change TBLSPCG
                continuous_convo = 2 : change TBLSPCG & adjust convolution filter size
                    default is 0

            *) key: python_accelerate, value: True or False
                For full python reconstruction (method : reconstruct), choose
                the way to reconstruct (optimized: True, non-optimized: False)
                    default is False

            *) key: threads, value int
                Number of thread for computing FFT when fftw is chosen
                    default is len(os.sched_getaffinity(0))

            *) key: opencl_init, value: True or False
                Enable the initialization of the plan with pre-computation
                of (U, V) for each (iu, iv) and filter values
                Computation full OpenCL (histogram + contribution plan)

            *) key: opencl_option, value: 1, 2, 3
                Choose the OpenCL kernel :
                    1 : atomic add with usual scheme
                    2 : prototype in Python with jmin, jmax, pmin, pmax
                            opencl_init is useless is this case
                    3 : pre-computation of plan during init + weighted sum kernel
                            opencl_init is useless is this case
                    default is 3
                See description of the option in the OpenCL implementation part

            *) key: opencl_custom_device, value: True or False
                Let you chose the platform & device of the OpenCL
                computation. The OpenCL menu will ask you to choose
                if enabled.
                    default is False (first device is chosen)

        """
        opt = options or {}
        self.add_dc_freq = opt.get("add_dc_freq", None) or False
        self.pswf_algrebra = opt.get("pswf_algebra", None) or False
        self.C = opt.get("C", None) or 7
        self.python_accelerate = opt.get("python_accelerate", None) or False

        self.continuous_convo = opt.get("continuous_convo", None) or 0
        self.custom_normalization = opt.get("custom_normalization", None) or None

        self.opencl_init = opt.get("opencl_init", None) or False
        self.opencl_option = opt.get("opencl_option", None) or 3
        self.opencl_custom_device = opt.get("opencl_custom_device", None) or False

        if not self.C == 7.0 and not self.pswf_algrebra:
            logging.warn(
                "If you choose a custom 'C', you should activate the computation of pswf by activating the option 'pswf_algrebra'"
            )

        self.threads = opt.get("threads", None) or len(
            os.sched_getaffinity(0)
        )  # current process pid = 0

    def _init_constants(self):
        """
        Initialize all constants, especially constants for PSWF
        (Prolate Spheroidal Wave Functions)
        """
        self.pol_approx_degree = 20  # maximal degree of legendre polynomial
        # in the approximation of the PSWF

        self.LAMBDA = 0.99998546  # PSWF stuff
        self.L = int(
            2 * self.C / np.pi
        )  # length of convolution filter, lambda_k * |alpha|^2
        self.L2 = int(self.C / np.pi)  # half of L... sometimes...

        assert self.L2 < 100

        self.LTBL = 512  # length of w_table, it is a wave function, w_table[0] = 1 and w_table[max] = 0
        self.TBLSPCG = 2 * self.LTBL / self.L  # multiplicator coefficient
        if self.continuous_convo > 0:
            self.TBLSPCG = 2 * self.LTBL / (2 * self.C / np.pi)  # new_tblspcg < tblspcg

        # to find apporpriate coef in w_table

        self.c_f_len = float(self.L2)  # convolution filter length
        if self.continuous_convo > 1:
            self.c_f_len = self.C / np.pi

        self.LEGENDRE_COEFS = np.array(
            [
                0.5767616e02,
                -0.8931343e02,
                0.4167596e02,
                -0.1053599e02,
                0.1662374e01,
                -0.1780527e-00,
                0.1372983e-01,
                -0.7963169e-03,
                0.3593372e-04,
                -0.1295941e-05,
                0.3817796e-07,
            ]
        )  # used to build w_table/w_inverse

    def _init_type(self, dtype):
        """
        Initialization and check of the input type

        Parameters
        ----------
        dtype: type
        """
        if dtype not in [np.float64, np.float32]:
            raise ValueError(
                "input dtype is not availaible, choose a type in [np.float64, np.float32]"
            )

        complex_dtype_mapping = {
            np.float64: np.complex128,
            np.float32: np.complex64,
        }

        self.dtype = dtype
        self.complex_dtype = complex_dtype_mapping[self.dtype]

    def _init_fft(self, backend_fft):
        available_backend = ["numpy", "fftw"]
        if backend_fft.lower() not in available_backend:  # add other fft option here
            raise ValueError(
                "backend for FFT not availaible, please choose between "
                + ", ".join(available_backend)
            )

        self.backend_fft = backend_fft
        logging.debug("  [Compute FFT plan with " + self.backend_fft + " backend]")

        if self.backend_fft == "fftw":  # storage of parameters for fftw
            fname = get_wisdom_file()  # should be located in /tmp
            import_wisdom(fname, on_mismatch="raise")

        # ---- Init plans for fft : ----
        # 1D iFFT :
        kwargs = {
            "shape": (self.n_angles, self.extended_width),
            "dtype": self.complex_dtype,
            "backend": self.backend_fft,
            "normalize": "none",
            "axes": (1,),
        }
        if self.backend_fft == "fftw":
            kwargs["num_threads"] = self.threads
        self.plan_ifft1d = fft.FFT(**kwargs)

        # 2D FFT :
        kwargs = {
            "shape": (self.extended_width, self.extended_width),
            "dtype": self.complex_dtype,
            "backend": self.backend_fft,
            "normalize": "none",
            "axes": (0, 1),
        }
        if self.backend_fft == "fftw":
            kwargs["num_threads"] = self.threads
        self.plan_fft2d = fft.FFT(**kwargs)

        if self.backend_fft == "fftw":
            export_wisdom(fname)

    def _init_geometry(self, sino_shape, pad_ratio, slice_shape, rot_center, angles):
        """
        Initialize shapes, rotation and angles

        Parameters
        ----------
        sino_shape: (n_angles, n_bins)
            Shape of the futur input sinogram
        slice_shape: (n_x, n_y) or n_x
            Shape of the output slice
        rot_center: scalar
            Center of rotation during acquisition
        angles: numpy array
            Array with all acquisition angles
        """
        if len(sino_shape) == 2:
            n_angles, dwidth = sino_shape
        else:
            raise ValueError("sino_shape must be a tuple of 2 integers")

        self.dwidth = dwidth  # detector width
        self.pad_ratio = pad_ratio
        self.extended_width = get_next_power(int(pad_ratio * dwidth))
        logging.debug("    Extended sinogram width = " + str(self.extended_width))
        self.rot_center = (
            rot_center or (self.dwidth - 1) / 2
        )  #  default is at the center
        self._set_angles(n_angles, angles)
        self._set_slice_shape(slice_shape)
        self._compute_final_padding()

    def _set_angles(self, n_angles, angles):
        """
        Copy angles if given or default: create equipartition in [0, PI[

        Parameters
        ----------
        n_angles: int
            Number of angles
        angles: numpy array
            Array with all acquisition angles
        """
        self.n_angles = n_angles
        if angles is None:  # default is equipartition in [0, PI[
            self.angles = np.linspace(
                0, np.pi, n_angles, endpoint=False, dtype=self.dtype
            )
        else:
            if len(angles) == n_angles:
                self.angles = angles
            else:
                raise ValueError(
                    "shape of array angles does not match with sinogram height"
                )

    def _set_slice_shape(self, slice_shape):
        """
        Set slice shape with given shape or default: set slice shape to detector width

        Parameters
        ----------
        slice_shape: (n_x, n_y) or n_x
            Shape of the output slice
        """
        n_y = self.dwidth
        n_x = self.dwidth  # default is dwidth
        if slice_shape is not None:
            if np.isscalar(slice_shape):
                slice_shape = (slice_shape, slice_shape)
            n_y, n_x = slice_shape
        self.n_x = n_x
        self.n_y = n_y
        self.slice_shape = (n_y, n_x)

    def _compute_final_padding(self):
        """
        Compute the padding of the output slice
        """
        self.pad_x = (self.extended_width - self.n_x) // 2
        self.pad_y = (self.extended_width - self.n_y) // 2

    def _compute_sum_legendre(self, degree, x):
        """
        Compute SUM(coefs(k)*P(2*k,x), for k=0,...,n/2)
        where P(j,x) is the jth Legendre polynomial.

        To understand why we need this sum :
        PSWF is a hard to compute. With some property,
        we know that we can approximate PSWF with
        legendre polynomials just with a few coefficient
        (LEGENDRE_COEFS).

        The approximation of the PSWF results in :
        PSWF = sum _0 ^+inf (LEGENDRE_COEFS[i] * legendre(i))
        where legendre(i) is the i-th polynomial of Legendre.
        We can prove that the odd coefficients are zeros,
        so we keep only the even coefficient.
        That is why, at the end,  LEGENDRE_COEFS[i] is the
        coefficient linked with legendre(2 * i).

        Parameters
        ----------
        n: int
            Degree of the sum
        x: scalar in [-1, 1]
        """
        res_sum = 0
        for i in range(0, degree // 2 + 1):
            res_sum += self.LEGENDRE_COEFS[i] * legendre(2 * i, x)

        return res_sum

    def _compute_pswf_tables(self):
        """
        The step of gridding is made with the PSWF theory.
        We construct 2 tables : w_table & w_inverse.
        - w_table is used during the gridding phase as a filter of
        convolution in the frequency space
        - w_inverse is used at the end to correct the convolution
        with w_table in the time space
        (Convolution in the frenquency space <=> multiplication in the
        time space)

        w_table's size is fixed at 512 & w_inverse's size is (extended_width - 1)

        PSWF are approximated with Legendre polynomial that is an orthogonal
        basis on [-1, 1] (PSWF = sum(coefs * L_i(x)) where odd coefficients are
        zeros)
        """
        linv = self.extended_width // 2 - 1
        fac = self.LTBL / (linv + 0.5)
        polyz = self._compute_sum_legendre(
            self.pol_approx_degree, 0.0
        )  # to normalized, w_table[0] = 1

        self.w_table = np.zeros((self.LTBL + 1))
        self.w_table[0] = 1

        self.w_table = np.array(
            [
                1
                if i == 0
                else self._compute_sum_legendre(self.pol_approx_degree, i / self.LTBL)
                / polyz
                for i in range(self.LTBL + 1)
            ],
            dtype=self.dtype,
        )

        # Normalization for 2D inverse FFT & scale factor
        # involved in the inverse Fourier transform of the convolvent
        if self.custom_normalization == None:
            norm = np.sqrt(np.pi / 2 / self.C / self.LAMBDA) / 1.2
        else:
            norm = self.custom_normalization

        # Construct W_inverse
        # self.w_inverse = np.zeros((2 * linv + 1)) --> bigger of 1 now, possible to apply on 2^n images
        self.w_inverse = np.zeros(self.extended_width, self.dtype)
        self.w_inverse[linv] = norm / self.w_table[0]
        for i in range(1, linv + 1):
            #  Minus sign for alternate entries corrects for "natural" data layout
            # in array H at end of Phase 1.
            norm = -norm
            curr = norm / self.w_table[int(np.round(i * fac))]
            self.w_inverse[linv + i] = curr
            self.w_inverse[linv - i] = curr
        self.w_inverse[self.extended_width - 1] = -norm / self.w_table[self.LTBL]
        assert np.shape(self.w_table) == (self.LTBL + 1,) and np.shape(
            self.w_inverse
        ) == (self.extended_width,)

    def _compute_pswf_tables_algebra(self):
        """
        Upgrade of _compute_pswf_tables

        The step of gridding is made with the PSWF theory.
        We construct 2 tables : w_table & w_inverse.
        - w_table is used during the gridding phase as a filter of
        convolution in the frequency space
        - w_inverse is used at the end to correct the convolution
        with w_table in the time space
        (Convolution in the frenquency space <=> multiplication in the
        time space)

        w_table's size is fixed at 512 & w_inverse's size is (extended_width - 1)

        PSWF are approximated with Legendre polynomial that is an orthogonal
        basis on [-1, 1] (PSWF = sum(coefs * L_i(x)) where odd coefficients are
        zeros)

        To determine coefs (= beta_0 here) we should solve an eigenequation.
        This equation is (A - eigenvalue * I)(beta) = 0
        The smallest eigenvalue gives the coefficients (=beta_0) for the
        first PSWF (=psi_0)
        And psi_0 = sum(beta_0[i] * L_i)
        """
        linv = self.extended_width // 2 - 1
        fac = self.LTBL / (linv + 0.5)
        self.w_table = np.zeros((self.LTBL + 1), dtype=self.dtype)

        # ------------------- Computation of the coefficients ----------------

        def _get_eig_pswf(A_size, c=self.C):
            A = np.zeros((A_size, A_size))
            for k in range(A_size):
                if k % 2 == 0:
                    A[k, k] = k * (k + 1) + (
                        (2 * k * (k + 1) - 1) / ((2 * k + 3) * (2 * k - 1))
                    ) * np.power(c, 2)
                    if k + 2 <= A_size - 1:
                        tmp = ((k + 1) * (k + 2) * np.power(c, 2)) / (
                            (2 * k + 3) * np.sqrt((2 * k + 5) * (2 * k + 1))
                        )
                        A[k + 2, k] = tmp
                        A[k, k + 2] = tmp
            w, v = np.linalg.eig(A)
            return w, v

        x_array = np.linspace(0, 1, len(self.w_table))

        A_size = (
            self.pol_approx_degree + 2
        )  # the size of A determine the number of coefficient of Legendre

        # get eigenvectors :
        _, v = _get_eig_pswf(A_size, c=self.C)

        # Get only the first PSWF (psi_0, j=0)
        beta_j = v[:, 0]  # coefficient in the sum psi_0 = sum(beta_j_i * L_i)
        for i, x in enumerate(x_array):  # for each point in [0, 1]
            for k in range(A_size):  # compute sum(b_k * sqrt(k + 0.5) * L_k(x))
                self.w_table[i] += beta_j[k] * np.sqrt(k + 0.5) * legendre(k, x)

        assert not self.w_table[0] == 0

        # Normalize w_table : self.w_table[0] = 1
        self.w_table = self.w_table / self.w_table[0]
        # ----------------------------------------------------------------------

        # integral_square_0_1 = np.trapz(np.power(self.w_table, 2), x_array)
        # norm = (
        #    np.sqrt(np.pi / 2 / self.C / self.LAMBDA) / 1.2
        # )

        if self.custom_normalization == None:
            # Law found with a linear regression to normalize :
            norm = 1 / np.sqrt(1.2468 * self.C - 0.105)
        else:
            norm = self.custom_normalization

        # Construct W_inverse
        self.w_inverse = np.zeros(self.extended_width, dtype=self.dtype)

        self.w_inverse[linv] = norm / self.w_table[0]

        for i in range(1, linv + 1):
            #  Minus sign for alternate entries corrects for "natural" data layout
            # in array H at end of Phase 1.
            norm = -norm
            curr = norm / self.w_table[int(np.round(i * fac))]
            self.w_inverse[linv + i] = curr
            self.w_inverse[linv - i] = curr

        # avoid last coefficient to be 0
        self.w_inverse[self.extended_width - 1] = -norm / self.w_table[self.LTBL]

    def _compute_main_filter(self, filter_name):
        """
        Main filter is computed with the chosen filter and an other coefficient
        This is the filter used on the sinogram before the convolution
        """
        # Note : previous filter was computed with compute_ramlak_filter(Nf, dtype=np.float32)
        # now with compute_ramlak_filter(Nf, dtype=np.float64)
        if filter_name is None:  # default
            filter_name = "ramlak"

        cutoff = 0.9  # cutoff frequency

        # ramlak_old & ramlak_dc_old are used to test non-regression
        if filter_name not in ["ramlak", "hamming", "ramlak_old", "ramlak_dc_old"]:
            raise ValueError("filter choice is not valid")

        assert self.extended_width % 2 == 0
        # else it there could be a problem with filter length

        # filter is symmetric, only keep the first half of the filter
        if filter_name in ["ramlak", "hamming"]:
            filter_fourier = compute_fourier_filter(
                self.extended_width, filter_name, cutoff
            )[0 : self.extended_width // 2]
        else:
            if filter_name == "ramlak_old":
                filter_fourier = filter_ramlak_old(self.extended_width)[
                    0 : self.extended_width // 2
                ]
            if filter_name == "ramlak_dc_old":
                filter_fourier = filter_ramlak_dc_old(self.extended_width)[
                    0 : self.extended_width // 2
                ]

        # Computation :
        norm = np.pi / self.extended_width / self.n_angles  # normalization
        tmp = 2 * np.pi * self.rot_center / self.extended_width

        x = np.arange(0, self.extended_width // 2, 1) * tmp
        # flip the + sign under compensate iFFT to FFT
        trigo = np.cos(x) + 1j * np.sin(x)  # e^(i2.pi.rot_c.x / N)

        self.filter = np.multiply((2 * filter_fourier), trigo, dtype=self.complex_dtype)
        self.filter = self.filter * norm

    # ---- OpenCL -----
    # This function has to be the same to compute the histogram
    # & to fill the contribution table (computation of iu/iv.h/l could change (float/double))
    def _iterate_on_iuv(self, function, *args):
        """
        Run function 'function' for each (iu, iv) founded
        """
        for p in range(self.n_angles):
            # -----------------------  DC  -------------------------
            if self.add_dc_freq:
                U = self.U_table[p, 0]
                V = self.V_table[p, 0]
                iul = int(max(ceil(U - self.dtype(self.c_f_len)), 1))
                iuh = int(
                    min(floor(U + self.dtype(self.c_f_len)), self.extended_width - 1)
                )
                ivl = int(max(ceil(V - self.dtype(self.c_f_len)), 1))
                ivh = int(
                    min(floor(V + self.dtype(self.c_f_len)), self.extended_width - 1)
                )

                for iu in range(iul, iuh + 1):
                    for iv in range(ivl, ivh + 1):
                        function(iu, iv, p, 0, *args)
            # ------------------------------------------------------

            # -----------------   Other Frequencies  ---------------
            for j in range(1, self.extended_width // 2):
                # U, V are the projected point (polar), they are
                # not on the cartesian grid
                U = self.U_table[p, j]
                V = self.V_table[p, j]

                # Compute window for convolution :
                # self.dtype(self.c_f_len) is important to replicate results
                # between OpenCL & Python
                # Without U - c_f_len is a double...
                iul = int(max(ceil(U - self.dtype(self.c_f_len)), 1))
                iuh = int(
                    min(floor(U + self.dtype(self.c_f_len)), self.extended_width - 1)
                )
                ivl = int(max(ceil(V - self.dtype(self.c_f_len)), 1))
                ivh = int(
                    min(floor(V + self.dtype(self.c_f_len)), self.extended_width - 1)
                )

                # Convolution loop :
                for iu in range(iul, iuh + 1):
                    for iv in range(ivl, ivh + 1):
                        function(iu, iv, p, j, *args)
            # ------------------------------------------------------

    def _init_histogram_opencl(self):
        histogram = np.zeros(
            (self.extended_width * self.extended_width + 1,), dtype=np.int32
        )  # still int32 for histogram non-cumulated
        # if around 500 000 angles, could be an overflow problem
        add_dc = 1 if self.add_dc_freq else 0
        # 1) Build histogram with OpenCL
        assert histogram.dtype == np.int32
        assert (
            self.dtype == np.float32
        )  # OpenCL computation is implemented only with simple precision
        assert self.U_table.dtype == self.dtype
        assert self.V_table.dtype == self.dtype

        d_hist = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, histogram.nbytes
        )
        d_U = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, self.U_table.nbytes
        )
        d_V = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, self.V_table.nbytes
        )

        cl.enqueue_copy(self.opencl_queue, d_hist, histogram)
        cl.enqueue_copy(self.opencl_queue, d_U, self.U_table)
        cl.enqueue_copy(self.opencl_queue, d_V, self.V_table)

        histo_gridsize = (np.int32(self.extended_width / 2 * self.n_angles),)
        # (np.int32(self.extended_width // 2), np.int32(self.n_angles))
        # Why it does not work with 2D grid ???

        histo_groupsize = None

        event = self.program_cl.gpu_histogram(
            self.opencl_queue,
            histo_gridsize,
            histo_groupsize,
            d_hist,
            d_U,
            d_V,
            np.int32(self.extended_width),
            np.int32(add_dc),
            np.float32(self.c_f_len),
        )
        # event.wait()
        # elapsed_time = 1e-9 * (event.profile.end - event.profile.start)
        # print("Execution time for histogram building : %g s" % elapsed_time)
        cl.enqueue_copy(self.opencl_queue, histogram, d_hist)

        # 2) Cumulated histogram

        # ... could be replace by np.cumsum(), yes but we keep
        # the same OpenCL pipeline to build histogram here,
        # otherwise, it could lead to different histogram
        # (especially for component (iu, iv)
        # where (iul, iuh, ivl, ivh) are almost integer) and we do
        # not want to fill the contribution array, with a different pipeline
        # than the one used to build the histogram

        # Cumulated histogram store the index where start the "list" (it is an array but
        # # the number of element is unknown, that is why we are building
        # the histogram) of (U, V) in the window of convolution of (iu, iv)
        # That is why the coefficient are shifted of 1 (hist[0] = 0 & hist[N] = total_contribution,
        # and |hist| = N+1)

        cumul_hist = np.insert(np.cumsum(histogram[:-1].astype(np.uint64)), 0, 0)
        assert cumul_hist.dtype == np.uint64

        return cumul_hist

    def _init_histogram(self):
        """
        Initialize a cumulated histogram

        Before cumulated :
        histogram[i] is the amount of (U, V) in the window
        of convolution of (iu, iv)

        After cumulated :
        histogram[i] is the index of access for contribution_table
        """
        # We can avoid building the histogram but we have to
        # store in list the contribution after ...
        # (do not know the size of each component)

        # 1) Build histogram
        histogram = np.zeros(
            (self.extended_width * self.extended_width + 1,), dtype=np.uint64
        )
        # last element is for total number of contribution

        def incr_histogram(iu, iv, p, j, histogram):
            if j == 0 and not self.add_dc_freq:
                return

            histogram[iu * self.extended_width + iv] += 1
            if not j == 0:
                histogram[
                    (self.extended_width - iu) * self.extended_width
                    + self.extended_width
                    - iv
                ] += 1

        # increment histogram for each (iu, iv) founded
        self._iterate_on_iuv(incr_histogram, histogram)

        # 2) Move to cumulated histogram
        # --> we get the index to access the array (0 first value)
        cumul_hist = np.insert(np.cumsum(histogram[:-1].astype(np.uint64)), 0, 0)
        assert cumul_hist.dtype == np.uint64

        return cumul_hist

    def _build_contribution_plan_opencl(self, cumul_histogram):
        total_contribution = cumul_histogram[self.extended_width * self.extended_width]
        contribution_table = np.zeros(
            (total_contribution, 2), dtype=np.int32
        )  # int32 for OpenCL & save memory
        contrib_filter_table = np.zeros(
            total_contribution, dtype=np.float32
        )  # save filter values (w[x] * w[y])
        copy_hist = deepcopy(
            cumul_histogram
        )  # we need a clean cumul_histogram to iterate

        assert cumul_histogram.dtype == np.uint64
        assert copy_hist.dtype == np.uint64
        assert contribution_table.dtype == np.int32
        assert contrib_filter_table.dtype == np.float32
        assert self.U_table.dtype == np.float32
        assert self.V_table.dtype == np.float32
        assert self.w_table.dtype == np.float32

        d_hist = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, cumul_histogram.nbytes
        )
        d_hist_copy = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, cumul_histogram.nbytes
        )
        d_contrib_table = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, contribution_table.nbytes
        )
        d_contrib_filter_table = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, contrib_filter_table.nbytes
        )
        d_U = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, self.U_table.nbytes
        )
        d_V = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, self.V_table.nbytes
        )
        d_w = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, self.w_table.nbytes
        )

        cl.enqueue_copy(self.opencl_queue, d_hist, cumul_histogram)
        cl.enqueue_copy(self.opencl_queue, d_hist_copy, copy_hist)
        cl.enqueue_copy(self.opencl_queue, d_contrib_table, contribution_table)
        cl.enqueue_copy(self.opencl_queue, d_contrib_filter_table, contrib_filter_table)
        cl.enqueue_copy(self.opencl_queue, d_U, self.U_table)
        cl.enqueue_copy(self.opencl_queue, d_V, self.V_table)
        cl.enqueue_copy(self.opencl_queue, d_w, self.w_table)

        histo_gridsize = (np.int32(self.extended_width / 2 * self.n_angles),)
        histo_groupsize = None
        add_dc = 1 if self.add_dc_freq else 0

        event = self.program_cl.gpu_fill_contrib_table(
            self.opencl_queue,
            histo_gridsize,
            histo_groupsize,
            d_hist,
            d_hist_copy,
            d_contrib_table,
            d_contrib_filter_table,
            d_U,
            d_V,
            d_w,
            np.int32(self.extended_width),
            np.int32(add_dc),
            np.float32(self.c_f_len),
            np.float32(self.TBLSPCG),
        )
        event.wait()
        elapsed_time = 1e-9 * (event.profile.end - event.profile.start)
        print("Execution time for contribution plans building : %g s" % elapsed_time)
        cl.enqueue_copy(self.opencl_queue, contribution_table, d_contrib_table)
        cl.enqueue_copy(self.opencl_queue, contrib_filter_table, d_contrib_filter_table)
        cl.enqueue_copy(
            self.opencl_queue, copy_hist, d_hist_copy
        )  # not necessary, just to check everythijng has been filled properly

        # check if everything has been filled properly :
        assert np.array_equal(
            copy_hist[: self.extended_width * self.extended_width], cumul_histogram[1:]
        )

        return contribution_table, contrib_filter_table

    def _build_contribution_plan(self, cumul_histogram):
        # 3) Create array (with -1) & use histogram to fill
        total_contribution = cumul_histogram[self.extended_width * self.extended_width]
        contribution_table = np.zeros(
            (total_contribution, 2), np.int32
        )  # int32 for OpenCL & save memory
        contrib_filter_table = np.zeros(
            total_contribution, np.float32
        )  # save filter values (w[x] * w[y])
        copy_hist = deepcopy(
            cumul_histogram
        )  # we need a clean cumul_histogram to iterate

        def _fill_contribution_table(
            iu,
            iv,
            p,
            j,
            cumul_histogram,
            copy_hist,
            contribution_table,
            contrib_filter_table,
        ):
            if j == 0 and not self.add_dc_freq:
                return

            U = self.U_table[p, j]  # re-access ...
            V = self.V_table[p, j]
            x = int(round(abs(U - iu) * self.TBLSPCG))
            y = int(round(abs(V - iv) * self.TBLSPCG))
            coeff_convolv = self.w_table[x] * self.w_table[y]
            """if iu * self.extended_width + iv == 650303:
                print(
                    "i = 1709: x",
                    x,
                    ",y",
                    y,
                    ",fabsx",
                    abs(U - iu),
                    ",fabsy",
                    abs(V - iv),
                )"""

            # First part :
            i = iu * self.extended_width + iv
            if not cumul_histogram[i] == cumul_histogram[i + 1]:  # not empty ?
                # we are not passing on the next part (allocation would be for another (iu, iv))
                assert copy_hist[i] < cumul_histogram[i + 1]
                # a) Fill contrib table with (p, j) :
                contribution_table[copy_hist[i], :] = (p, j)

                # b) Pre-compute filter value :
                contrib_filter_table[copy_hist[i]] = coeff_convolv

                copy_hist[i] += 1

            # Symmetric part :
            i2 = (
                (self.extended_width - iu) * self.extended_width
                + self.extended_width
                - iv
            )
            if not cumul_histogram[i2] == cumul_histogram[i2 + 1]:
                if not j == 0:
                    # we are not passing on the next part (spacde for another (iu, iv))
                    assert copy_hist[i2] < cumul_histogram[i2 + 1]
                    # a) Fill contrib table with (p, j) :
                    contribution_table[copy_hist[i2], :] = (p, self.extended_width - j)

                    # b) Pre-compute filter value :
                    contrib_filter_table[copy_hist[i2]] = coeff_convolv

                    copy_hist[i2] += 1

        self._iterate_on_iuv(
            _fill_contribution_table,
            cumul_histogram,
            copy_hist,
            contribution_table,
            contrib_filter_table,
        )

        # check if everything has been filled properly :
        assert np.array_equal(
            copy_hist[: self.extended_width * self.extended_width], cumul_histogram[1:]
        )

        return contribution_table, contrib_filter_table

    def _init_plans_opencl(self):
        # ------ Initialize OpenCL context ------
        logging.debug("    Init OpenCL context")
        self.opencl_context = None

        if not self.opencl_custom_device:
            platform = cl.get_platforms()[0]  # Select the first platform [0]
            device = platform.get_devices()[
                0
            ]  # Select the first device on this platform [0]
            self.opencl_context = cl.Context(
                [device]
            )  # Create a context with your device
        else:  # menu will appear, and user will have to choose
            self.opencl_context = cl.create_some_context()

        self.opencl_queue = cl.CommandQueue(
            self.opencl_context, properties=cl.command_queue_properties.PROFILING_ENABLE
        )

        # ------ Build and execute the kernel ------
        kernel_file = os.path.join(
            "gridrec/opencl",
            "gridding.cl",  # TODO change this absolute path, only work if user in correct dir
        )
        kernel_src = open(kernel_file).read()
        self.program_cl = cl.Program(self.opencl_context, kernel_src).build()
        self.gridsize = (np.int32(self.extended_width), np.int32(self.extended_width))
        self.groupsize = None

        # To build histogram and computation plan, it is better
        # to keep a unique language pipeline (full-python or full-opencl)
        # because computation between python & openCL could create little gap
        # (float, double) and the histogram could be different in both case.
        # Then, if you fill the contribution table with an other pipeline,
        # there could be filling overflow...

        # ------ Compute histogram and plans ------
        logging.debug("    Building histogram")
        # We get the cumulated histogram, it should be uint64
        self.cumulated_histogram = self._init_histogram_opencl()

        logging.debug("    Building contribution plans")
        (
            self.contribution_table,
            self.contrib_filter_table,
        ) = self._build_contribution_plan_opencl(self.cumulated_histogram)

        # Following comments used to compare OpenCL & Python during implementation :
        #             ----------------------------------------------
        # Cannot do array comparaison, because the order of the contribution
        # could have changed, indeed (parallel computation of the contribution)
        # Check sum for each (iu, iv) :
        """for i in range(self.extended_width * self.extended_width):
            s1 = np.sum(
                contribution_table_opencl[self.cumulated_histogram[i] : self.cumulated_histogram[i + 1]]
            )
            s2 = np.sum(
                self.contribution_table[self.cumulated_histogram[i] : self.cumulated_histogram[i + 1]]
            )
            # print(s1, s2)
            s1f = np.sum(
                contrib_filter_table_opencl[self.cumulated_histogram[i] : self.cumulated_histogram[i + 1]]
            )
            s2f = np.sum(
                self.contrib_filter_table[self.cumulated_histogram[i] : self.cumulated_histogram[i + 1]]
            )
            # print("i :", i)
            # print(s1f, s2f)
            assert np.array_equal(s1, s2)
            assert abs(s1f - s2f) < 1e-2  # 1e-5"""

        # ------ Create device arrays and copy to GPU ------
        # (in & out for nothing here, we could keep everything on GPU memory
        # it just allows to use the python prototype)
        logging.debug("    Copy plans on GPU memory")
        assert self.cumulated_histogram.dtype == np.uint64
        assert self.contribution_table.dtype == np.int32
        assert self.contrib_filter_table.dtype == np.float32
        self.d_hist = cl.Buffer(
            self.opencl_context,
            cl.mem_flags.READ_WRITE,
            self.cumulated_histogram.nbytes,
        )
        self.d_contrib = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, self.contribution_table.nbytes
        )
        self.d_contrib_filter = cl.Buffer(
            self.opencl_context,
            cl.mem_flags.READ_WRITE,
            self.contrib_filter_table.nbytes,
        )
        cl.enqueue_copy(self.opencl_queue, self.d_hist, self.cumulated_histogram)
        cl.enqueue_copy(self.opencl_queue, self.d_contrib, self.contribution_table)
        cl.enqueue_copy(
            self.opencl_queue, self.d_contrib_filter, self.contrib_filter_table
        )

    # -----------------  Reconstruction methods : ------------------------

    def _check_sino(self, sino):
        """
        Check sinogram length
        """
        if not np.shape(sino) == (self.n_angles, self.dwidth):
            raise ValueError(
                "Shape of given sinogram does not match parameters with the reconstructor"
            )

        if not sino.dtype == self.dtype:
            raise TypeError("Sinogram dtype does not match with initialized dtype")

    def _padded_sinogram(self, sino):
        """
        Return a padded sinogram with all 0 on the right with width = extended_width
        """
        return np.pad(
            sino,
            pad_width=[(0, 0), (0, self.extended_width - self.dwidth)],
        )

    def _fft_sinogram(self, array):
        """
        Compute a iFFT on each line of array, without usual normalization
        """
        # Ususaly normalization is :
        # FFT -> no normalization : sum(...)
        # iFFT -> normalization : 1/N * sum(...)
        # --> here we do iFFT without normalization : 1 * sum(...)

        # finally, still a normalization needed,
        # and not the same for numpy and fftw
        # looks like silx does not normalize it well for numpy backend
        # update : silx fft normalization in pull request, may need to change
        # the normalization here one day !
        # i.e remove '* array.shape[1] (* array.shape[1])'

        res = self.plan_ifft1d.fft(array).astype(self.complex_dtype)  # .conjugate()

        if self.backend_fft == "fftw":
            return res * array.shape[1]
        if self.backend_fft == "numpy":
            return res * array.shape[1] * array.shape[1]
        return res.astype(self.complex_dtype)

    def _ifft2_image(self, img):
        """
        Compute a FFT-2D on img
        """
        return self.plan_fft2d.ifft(img).astype(self.complex_dtype)

    def _fft_shift(self, img):
        """
        Compute the shifted image
        """
        return np.fft.fftshift(img)

    def _apply_corrections(self):
        """
        The convolution in _pswf_convolution_gridding needs to be corrected here
        after the 2D-FFT. For each pixel in the cropped image, apply the correction.

        Return
        -------
        image: numpy.ndarray
            Cropped_image times w_inverse element-wise
        """
        assert (self.n_x, self.n_y) == np.shape(self.cropped_image)

        # for (x, y): recon[x, y] = w_inverse[padx + x] * w_inverse[pady + y] * cropped_img[x, y].real
        correction_table = np.outer(
            self.w_inverse[self.pad_x : self.pad_x + self.n_x],
            self.w_inverse[self.pad_y : self.pad_y + self.n_y],
        )

        recon = np.multiply(
            (self.cropped_image).real, correction_table, dtype=self.dtype
        )

        return recon

    def _rotation_90(self, img):
        """
        Compute the rotation of 90 degrees of the image
        """
        return np.rot90(img, k=1)

    ###############################################
    # Main of the algorithm is the same for every
    # options, but there are steps that have different
    # ways of reconstruction. It is the case for the
    # gridding convolution for example. Below are the
    # functions that changed according to the option/way
    # of reconstruction.
    #
    # There are 3 ways of reconstruction :
    # 1] full python non-optimized (slow)
    # --> reconstruct (option : python_accelerate = False)
    # 2] full python optimized (fast)
    # --> reconstruct (option : python_accelerate = True)
    # 3] python & c (very fast)
    # --> reconstruct_c

    ########## 1] Full Python Non-Optimized ##########
    def _pswf_convolution_gridding_non_opt(self):
        """
        Convolution process to switch from polar coordinates to a cartesian grid.
        For each frequency in the transformed sinogram, compute (U, V) with (p,j)
        and convolve w[x].w[y] (of size L) with the filterred coefficient of the sinogram sino[p, j].
        Each frequency give a small contribution around (U, V). The indices iu & iv are located
        in a square around (U, V).

        The calculation is parallel for both semi-disk in the new tranformed image H.

        ----  Whole resulting frequency image : ----

        V
        ^
        +------------------------------+
        |
        |       (H[iu, iv])      (U,V)
        |                          *
        |
        +--------------j0---------------+
        |
        |
        |   (H[ext_width-iu, ext_width-iv])
        |
        |
        +------------------------------+---> U

        This is parallel (1 loop for 2 contribution) because of the symmetry,
        but the case j=0 need to be done
        separatly. Indeed, this is the DC and a high frequency,
        there is no symetry in this case :
        DC is at the center and high frequency is around the disk.


        ---- Zoom in : ---- (Convolution step)


        (iul, ivl) |      |      |
        ----x------x------x------x----
            |      |      |      |
            |      |      |      |
        ----x------x------x------O-(iu, iv)
            |      |  *   |      |
            |      |(U,V) |      |
        ----x------x------x------x----
            |      |      |      |
            |      |      |      |
        ----x------x------x------x----
            |      |      |  (iuh, ivh)


        For each (U,V) in polar coordinates (r.cos(theta), r.sin(theta)),
        add the contribution to each cartesian grid point represented with x
        Current grid point is (iu, iv) and border are (iul, ivl) & (iuh, ivh)
        Contribution is calculated as a "convolution". It is the product
        of the value in (U,V) and the filter (PSWF).
        """
        H = np.zeros(
            (self.extended_width, self.extended_width), dtype=self.complex_dtype
        )  # inverse resulted image

        for p in range(self.n_angles):
            if p % 50 == 0:  # logging
                logging.debug("      --> Convolution p= " + str(p) + " ...")

            # Look "Other Frequencies" below for comments
            # -----------------------  DC  -------------------------
            if self.add_dc_freq:
                U = self.U_table[p, 0]
                V = self.V_table[p, 0]
                c_data_1 = self.filter[0] * self.sino[p, 0]
                iul = int(max(np.ceil(U - self.c_f_len), 1))
                iuh = int(min(np.floor(U + self.c_f_len), self.extended_width - 1))
                ivl = int(max(np.ceil(V - self.c_f_len), 1))
                ivh = int(min(np.floor(V + self.c_f_len), self.extended_width - 1))

                for iu in range(iul, iuh + 1):
                    for iv in range(ivl, ivh + 1):
                        x = int(np.round(np.abs(U - iu) * self.TBLSPCG))
                        y = int(np.round(np.abs(V - iv) * self.TBLSPCG))
                        convolv_coef = self.w_table[x] * self.w_table[y]
                        c1 = convolv_coef * c_data_1
                        H[iu, iv] += c1
            # ------------------------------------------------------

            # --------------------   High Freq  --------------------
            # Contribution is very small, skip that frequency
            # In case we want to add it, link with these coefs :
            # self.filter[?] : index is len / 2 but it is out of bound
            # self.sino[p, len / 2]
            # ------------------------------------------------------

            # -----------------   Other Frequencies  ---------------
            for j in range(1, self.extended_width // 2):
                # U, V are the projected point (polar), they are
                # not on the cartesian grid
                U = self.U_table[p, j]
                V = self.V_table[p, j]

                # Get filtered sinogram data, value in (U,V)
                c_data_1 = self.filter[j] * self.sino[p, j]  # filtered sinogram datas
                c_data_2 = (
                    np.conjugate(self.filter[j]) * self.sino[p, self.extended_width - j]
                )

                # Compute window for convolution :
                iul = int(max(np.ceil(U - self.c_f_len), 1))
                iuh = int(min(np.floor(U + self.c_f_len), self.extended_width - 1))
                ivl = int(max(np.ceil(V - self.c_f_len), 1))
                ivh = int(min(np.floor(V + self.c_f_len), self.extended_width - 1))

                # Note : contribution to (u, v) where u = 1 or v = 1 is impossible,
                # it is only in the upper odd square of size (ext_width - 1, ext_width - 1)

                # Convolution loop :
                for iu in range(iul, iuh + 1):
                    for iv in range(ivl, ivh + 1):
                        # Calculation of the index of access in w_table
                        # computation of the filter value
                        x = int(round(abs(U - iu) * self.TBLSPCG))
                        y = int(round(abs(V - iv) * self.TBLSPCG))

                        convolv_coef = self.w_table[x] * self.w_table[y]

                        c1 = convolv_coef * c_data_1
                        c2 = convolv_coef * c_data_2

                        # 2 operations at the same time with symmetry:
                        # (see graph above to understand)
                        H[iu, iv] += c1
                        H[self.extended_width - iu, self.extended_width - iv] += c2
            # ------------------------------------------------------

        return H

    ############ 2] Full Python Optimized ############
    def _get_iuv(self, U, V):
        """
        Get border of the convolution from (U, V) point
        """
        assert U.dtype == self.dtype
        assert V.dtype == self.dtype

        iul = int(max(ceil(U - self.c_f_len), 1))  # self.C / np.pi
        iuh = int(min(floor(U + self.c_f_len), self.extended_width - 1))
        ivl = int(max(ceil(V - self.c_f_len), 1))
        ivh = int(min(floor(V + self.c_f_len), self.extended_width - 1))

        return iul, iuh, ivl, ivh

    def _compute_indices(self, grid_points):
        """
        Compute indices in a huge vector to accelerate
        """
        self.indices_xy = np.rint(np.abs(grid_points) * self.TBLSPCG).astype("int")

    def _compute_filtered_sinogram(self):
        """
        Filter the sinogram in place with filter of the reconstructor
        """
        assert self.filter.dtype == self.complex_dtype
        complete_filter = np.concatenate(
            (
                self.filter,
                [0],
                np.flip(self.filter).conjugate()[0 : self.extended_width // 2 - 1],
            ),
            dtype=self.complex_dtype,
        )  # [0] is the coefficient for the highest frequency ignored here
        # self.filter[0] is the coefficient for the DC frequency and then
        # the other frequency are symmetrics with coefficients at :
        # (self.complete_filter[j], self.complete_filter[len -j])
        # for j = 1, ..., len/2 - 1
        assert complete_filter.dtype == self.complex_dtype
        np.multiply(self.sino, complete_filter, out=self.sino, dtype=self.complex_dtype)

    def _compute_convolution_indices(self):
        """
        Compute every points that will be part of the convolution
        and get the corresponding indices (those indices are
        defined on the distance between the grid point and
        (U, V) and thoses indices will give the valur of the filter
        of convolution for each point)
        """
        # grid_point store all the position of the point of the convolution
        # --> it will become the indices of access in w_table
        # convolution_border store all the border for each pair (p, j)

        grid_points = []
        self.max_convolution_filter_size = 0
        self.convolution_border = np.zeros(
            (self.n_angles, self.extended_width // 2, 4), dtype=np.int64
        )

        for p in range(self.n_angles):
            for j in range(0, self.extended_width // 2):
                U = self.U_table[p, j]
                V = self.V_table[p, j]

                assert U.dtype == self.dtype
                assert V.dtype == self.dtype

                iul, iuh, ivl, ivh = self._get_iuv(U, V)

                self.max_convolution_filter_size = max(
                    self.max_convolution_filter_size, iuh - iul + 1, ivh - ivl + 1
                )  # max is 2 * L2 + 1

                grid_points.append(U - np.arange(iul, iuh + 1))
                grid_points.append(V - np.arange(ivl, ivh + 1))

                self.convolution_border[p, j] = iul, iuh, ivl, ivh

        # 2) Apply abs, round, ... to get the indexes
        grid_points = np.concatenate(grid_points)

        self._compute_indices(grid_points)

    def _build_filter_and_convolve(self):
        """
        Build an extended array containing the separate 1D filter
        k_x, k_y for each angle. Compute the outer product of each
        pair (k_x, k_y)
        """
        # Select all the coefficient at the good indices :
        convolve_coefs = np.take(self.w_table, self.indices_xy)

        H = np.zeros(
            (self.extended_width, self.extended_width), dtype=self.complex_dtype
        )  # inverse resulted image

        # indexes to go through convolve_coefs array, the size is not regular due
        # to irregular size of filter k_x, k_y
        curr_index = (
            0  # contain case j=0, should be incremented even if DC is not added
        )
        second_index = 0

        for p in range(self.n_angles):

            # ----------- Compute all filter k for a given p ---------
            k_x_extended = np.zeros(
                (self.extended_width // 2, self.max_convolution_filter_size),
                dtype=self.dtype,
            )
            k_y_extended = np.zeros(
                (self.extended_width // 2, self.max_convolution_filter_size),
                dtype=self.dtype,
            )
            for j in range(0, self.extended_width // 2):
                iul, iuh, ivl, ivh = self.convolution_border[p, j]

                k_x_size = iuh - iul + 1
                k_x = convolve_coefs[second_index : second_index + k_x_size]
                k_x_extended[j, 0:k_x_size] = k_x
                second_index += k_x_size

                k_y_size = ivh - ivl + 1
                k_y = convolve_coefs[second_index : second_index + k_y_size]
                k_y_extended[j, 0:k_y_size] = k_y
                second_index += k_y_size
            # ---------------------------------------------------------

            # https://stackoverflow.com/questions/47624948/calculating-the-outer-product-for-a-sequence-of-numpy-ndarrays
            # Calculation of the outer products at the same time,
            # we get all the np.outer(k_x, k_y) for the different
            # j in the same time
            k_extended = np.multiply(
                k_x_extended[..., None], k_y_extended[:, None, :], dtype=self.dtype
            )  # extend size and use broadcasting
            k_extended_fliped = np.flip(k_extended, axis=(1, 2))

            # -------------- Apply each filter -------------------------
            for j in range(0, self.extended_width // 2):
                iul, iuh, ivl, ivh = self.convolution_border[p, j]
                k_x_size = iuh - iul + 1
                curr_index += k_x_size
                k_y_size = ivh - ivl + 1
                curr_index += k_y_size

                if (
                    not j == 0 or self.add_dc_freq
                ):  # goes for every j and for j==0 if add_dc_freq == True
                    # get sub-plan one by one
                    k_from_extended_filter = k_extended[j, :k_x_size, :k_y_size]
                    k_from_extended_filter_fliped = k_extended_fliped[
                        j,
                        self.max_convolution_filter_size - k_x_size :,
                        self.max_convolution_filter_size - k_y_size :,
                    ]

                    H[iul : iuh + 1, ivl : ivh + 1] += (
                        k_from_extended_filter * self.sino[p, j]
                    )
                    if not j == 0:  # symetric
                        H[
                            self.extended_width - iuh : self.extended_width - iul + 1,
                            self.extended_width - ivh : self.extended_width - ivl + 1,
                        ] += (
                            k_from_extended_filter_fliped
                            * self.sino[p, self.extended_width - j]
                        )
            # -----------------------------------------------------------

            if p % 100 == 0:
                logging.debug("      " + str(round(p / self.n_angles * 100)) + "% ...")

        assert H.dtype == self.complex_dtype
        return H

    # Pseudo-code of a convolution step :
    # (easier to understand than the optimized code)
    #
    # def _convolution_step(self, p, j, H):
    #   Comupte point position and
    #   (U, V) = j * cos(angle[p]) + len / 2, j * sin(angle[p]) + len / 2
    #   data1 = filter * sino [p, j]
    #   data2 = filter * sino [p, n - j]
    #
    #   Compute border (window of this convolution step):
    #   iul, iuh, ivl, ivh
    #
    #   Separation between x and y
    #   k_x = w_table[indices ~ distance on x axis btw cartesian point and (U, V)]
    #   k_y = ...
    #
    #   k = outer product of k_x and k_y (filter 2D)
    #
    #   H[iul : iuh + 1, ivl : ivh + 1] += k * data1
    #   if j != 0:
    #       H[len - iuh : len - iul + 1, len - ivh : len - ivl + 1] += flip(k) * data2

    def _pswf_convolution_gridding_opt(self):
        """
        See _pswf_convolution_gridding_opt for details
        """
        # Apply filter on self.sino
        self._compute_filtered_sinogram()  # filtered sinogram is still self.sino

        # Then compute the convolution between the filtered sinogram
        # and the filter build with w_table
        # We need to compute the indices of access into w_table
        # based on the distance between each pair (U, V) and
        # the grid points around
        # To do it faster, we first compute all the indices using numpy
        # vectorization speed :
        self._compute_convolution_indices()

        # and then we build the filter of convolution using a huge
        # tensor product, and finally convolving :
        return self._build_filter_and_convolve()

    ####################################################
    ########## 1&2] Full Python Non-Optimized ##########
    ################### & Optimized ####################

    def reconstruct(self, input_sino):
        """
        Construct the reconstruction with the
        gridrec algorithm (Gridding Reconstruction method)

        Parameters
        ------------
        input_sino: sinogram of size previously given in the reconstructor initialization

        Returns
        ------------
        reconstruction with the size given in the reconstructor initialization
            type : np.float32
        """
        logging.info("*** Python GRIDREC reconstruction ***")

        ######  [Phase 1]  ######
        logging.debug("  [Phase 1]")
        self._check_sino(input_sino)
        logging.debug("    [Pad sinogram]")
        self.sino = self._padded_sinogram(input_sino)
        # FFT on each lines to go in Fourier space
        logging.debug("    [FFT-1D]")
        self.sino = self._fft_sinogram(self.sino.astype(self.complex_dtype))
        # At the end of this step, sino_fft contains the 1D-FFT
        # of the input sinogram, with DC at index 0 and negative frequency
        # in the second part of the array.

        logging.debug("    [PSWF gridding]")
        if self.python_accelerate:
            self.fourier_transformed_image = self._pswf_convolution_gridding_opt()
        else:
            self.fourier_transformed_image = self._pswf_convolution_gridding_non_opt()
        # At the end of Phase1, fourier_transformed_image
        # (size : ext_width * ext_width) contains the inverse of
        # the reconstructed image.

        ######  [Phase 2]  ######
        logging.debug("  [Phase 2]")
        logging.debug("    [iFFT-2D]")
        # Central frequencies are at the center of fourier_transformed_image
        self.image = self._ifft2_image(self.fourier_transformed_image)
        # At the end of Phase 2, the real part of the image
        # contains the reconstruction
        # but we need to re-order the pixels and to apply a
        # correction factor later in Pahse 3.

        ######  [Phase 3]  ######
        logging.debug("  [Phase 3]")
        logging.debug("    [FFT shift & crop]")
        # Central frequencies where at the center, so we need a shift :
        self.image = self._fft_shift(self.image)
        self.cropped_image = self.image[
            self.pad_x : (self.pad_x + self.n_x), self.pad_y : (self.pad_y + self.n_y)
        ]
        # At the end of this step, cropped_image has the same size than
        # the reconstructed image. It is already near the targetting
        # reconstructed image, but we need a few corrections due to
        # the usage of PSWF above.

        logging.debug("    [Apply corrections]")
        # The usage of PSWF in Phase 1, was in the frenquency
        # space. We need to shut down the effect in the spatial space.
        # Fourier[H * W]/ w --> Fourier[H * w_table] * w_inverse
        self.recon = self._apply_corrections()
        # Final rotation because first slice was positionned
        # horizontally during gridding when it start usually
        # vertically on other algorithm
        self.recon = self._rotation_90(self.recon)

        logging.debug("*** Python GRIDREC end reconstruction ***")

        return self.recon.astype(np.float32)

    ####################################################
    ################# 3] Python & C ####################
    ####################################################
    # Calling C functions to accelerate the process
    def _pswf_convolution_gridding_c(self):
        """
        Same as _pswf_convolution_gridding but with a binding in C
        It uses pybind11 to do the connection between C and python
        """
        self._compute_filtered_sinogram()  # self.sino contains the filtered sinogram

        add_dc = 1 if self.add_dc_freq else 0

        assert self.sino.dtype == self.complex_dtype
        assert self.U_table.dtype == self.dtype
        assert self.V_table.dtype == self.dtype
        assert self.w_table.dtype == self.dtype

        # Call C function here :
        if self.dtype == np.float32:

            H = pswf_gridding_c.pswf_convolution_gridding_float(
                self.sino,
                self.U_table,
                self.V_table,
                self.w_table,
                int(self.L2),
                int(self.TBLSPCG),
                add_dc,
            )
        else:
            H = pswf_gridding_c.pswf_convolution_gridding(
                self.sino,
                self.U_table,
                self.V_table,
                self.w_table,
                int(self.L2),
                int(self.TBLSPCG),
                add_dc,
            )

        return H

    def reconstruct_c(self, input_sino):
        """
        Construct the reconstruction with the
        gridrec algorithm (Gridding Reconstruction method)
        This function is faster than reconstruct because it is accelerated by
        C binding

        Parameters
        ------------
        input_sino: sinogram of size previously given in the reconstructor initialization

        Returns
        ------------
        reconstruction with the size given in the reconstructor initialization
            type : np.float32
        """
        if not __have_c_gridrec__:
            raise RuntimeError(
                "reconstruct_c is not available, you may not have pybind11 installed"
            )

        logging.info("*** Python GRIDREC reconstruction C ***")
        # See reconstruct to get comments

        logging.debug("  [Phase 1]")
        self._check_sino(input_sino)
        logging.debug("    [Pad sinogram]")
        self.sino = self._padded_sinogram(input_sino)
        logging.debug("    [FFT-1D]")
        self.sino = self._fft_sinogram(self.sino.astype(self.complex_dtype))
        logging.debug("    [PSWF gridding]")
        self.fourier_transformed_image = self._pswf_convolution_gridding_c()

        logging.debug("  [Phase 2]")
        logging.debug("    [iFFT-2D]")
        self.image = self._ifft2_image(self.fourier_transformed_image)

        logging.debug("  [Phase 3]")
        logging.debug("    [FFT shift & crop]")
        self.image = self._fft_shift(self.image)
        self.cropped_image = self.image[
            self.pad_x : (self.pad_x + self.n_x), self.pad_y : (self.pad_y + self.n_y)
        ]

        logging.debug("    [Apply corrections]")
        self.recon = self._apply_corrections()
        self.recon = self._rotation_90(self.recon)

        logging.debug("*** Python GRIDREC end reconstruction C ***")

        return self.recon.astype(np.float32)

    #######################################################
    ################# 4] Python & OpenCL ##################
    #######################################################

    # Work in progress...
    # Explainations :

    # Option 1 uses atomic operations, it does not cost a lot of memory ('small'
    # tables (U, V, w, ...) & sinogramm) but the repartition of contribution
    # is concentrated at the center (more polar point at the center), so atomic
    # could become a problem for the addition at the center.

    # Options 2 --> too slow

    # Options 3 the computation is fast, but the transfer of data is slow (enqueue_copy)
    # indeed, copying data costs a little more than computing the gridding.
    # Moreover, the size of 'contribution_table' and 'contrib_filter_table'
    # are huge. Those tables are the same for every slices.
    # The problem could be that if we want to pre-copy some sinogram and doing the
    # computation of the griddding in parallel, we will not be able to store a lot
    # of sinogram as long as the size of the 'contrib_table' are already using all
    # the memory for big images.
    # 'contrib_filter_table' is not necessary, we can re-compute the weight of the
    # convolution each time (bandwidth GPU memory <-> device is quite fast)
    # but the other table 'contribution_table' is necessary to get the repartition of
    # contribution.
    # At this point, transfer of data is too slow, and it has not normal values :
    # to transfer 36Mo, it takes 120ms, where it should take 6ms if bandwidth
    # host <-> device is 6GB/s... problem of latency ? (fct enqueue_copy())
    # (tested both on GPU (scisoft14) and CPU, same problem)
    # An other problem could be the size of the 'contrib_tables', as mentions before
    # those are huge, and if the dimension of the extended image are too large,
    # it could even be too large to fit into the GPU memory.
    # For this option, oclgrind seems to not have any problem with it :)

    ###
    ### Option 3) --> better one
    ###
    ### Init histogram and plan during initialization (atomic operations only once),
    ### Then just multiply stored coefficient with filtered sinogram
    def _compute_fourier_image(
        self, histogram, contribution_table, contrib_filter_table
    ):
        """
        Prototype for _pswf_convolution_gridding_opencl()
        State : unused
        """
        H = np.zeros(
            (self.extended_width, self.extended_width), dtype=self.complex_dtype
        )  # inverse resulted image

        for iu in range(self.extended_width):
            for iv in range(self.extended_width):
                # kernel  in OpenCL :
                j = iu * self.extended_width + iv
                for i in range(histogram[j], histogram[j + 1]):

                    # add contribution of element at indexs i
                    p, j = contribution_table[i]

                    c_data_1 = self.sino[p, j]
                    if not j == 0:
                        c_data_2 = self.sino[p, self.extended_width - j]

                    convolv_coef = contrib_filter_table[i]

                    H[iu, iv] += convolv_coef * c_data_1
                    if not j == 0:
                        H[self.extended_width - iu, self.extended_width - iv] += (
                            convolv_coef * c_data_2
                        )
        return H

    def _pswf_convolution_gridding_opencl(self):
        """
        Same as _pswf_convolution_gridding but with OpenCL backend
        Contribution plan should be initialize before, this plan
        store the filter coefficients and the sinogram coordinates (p,j)
        for each contribution (for each (U, V) inside the window
        of convoution of (iu, iv))
        """
        assert self.dtype == np.float32  # only in float32 for now
        if not self.opencl_init:
            raise ValueError(
                "You use OpenCL but you did not activate the 'opencl_init' option, please activate this option"
            )

        self._compute_filtered_sinogram()  # self.sino contains the filtered sinogram
        add_dc = 1 if self.add_dc_freq else 0
        H = np.zeros(
            (self.extended_width, self.extended_width), dtype=self.complex_dtype
        )  # inverse resulted image

        assert self.sino.dtype == self.complex_dtype

        d_sino = cl.Buffer(
            self.opencl_context, cl.mem_flags.READ_WRITE, self.sino.nbytes
        )
        d_H = cl.Buffer(self.opencl_context, cl.mem_flags.READ_WRITE, H.nbytes)

        # Copy on the GPU
        cl.enqueue_copy(self.opencl_queue, d_sino, self.sino)
        cl.enqueue_copy(self.opencl_queue, d_H, H)

        logging.debug("      Compute the gridding")

        event = self.program_cl.gpu_gridding(
            self.opencl_queue,
            self.gridsize,
            self.groupsize,
            d_sino,
            self.d_contrib,
            self.d_contrib_filter,
            self.d_hist,
            d_H,
            np.int32(self.extended_width),
            np.int32(add_dc),
        )
        event.wait()
        elapsed_time = 1e-9 * (event.profile.end - event.profile.start)
        print(
            "Execution time for gridding computation : %g s" % elapsed_time
        )  # warning, only computing time

        # Copy results
        cl.enqueue_copy(self.opencl_queue, H, d_H)

        return H

        # In Python :
        # H = self._compute_fourier_image(
        #    self.cumulated_histogram, self.contribution_table, self.contrib_filter_table
        # )

    ###
    ### Option 2) --> too slow
    ###
    ### Compute coefficient in cartesian space
    ### by computing polar window (jmin, jmax, pmin, pmax)
    def _pswf_convolution_gridding_proto_opencl(self):
        """
        Prototype
        For each (iu, iv), determine area (radius & angles) to find neighbor (U, V)
        Then search in this area and add contributions
        Searching for every (iu, iv) point is very slow
        """
        # Not exactly the same as original implementation in Python
        # ex : iu = 1, iv = 558
        # --> float + round differences ...

        self._compute_filtered_sinogram()  # self.sino contains the filtered sinogram

        H = np.zeros(
            (self.extended_width, self.extended_width), dtype=self.complex_dtype
        )  # inverse resulted image

        nb_contribution = 0

        for iu in range(1, self.extended_width - 1):
            for iv in range(1, self.extended_width - 1):
                # Find the window in which are the (U, V) neighbors of (iu, iv)

                # Condition on j (radius)
                # condition on p if window is not on an axis

                p = (0, 0)  # origine
                rect = (  # window of convolution with origine at the center of H
                    (
                        iu - self.c_f_len - self.extended_width // 2,
                        iv - self.c_f_len - self.extended_width // 2,
                    ),
                    (
                        iu + self.c_f_len - self.extended_width // 2,
                        iv + self.c_f_len - self.extended_width // 2,
                    ),
                )

                j_high = int(ceil(max_dist_to_rect(rect, p)))
                j_low = int(floor(min_dist_to_rect(rect, p)))

                if not self.add_dc_freq:  # take continuous frequency ?
                    j_low = max(1, j_low)
                j_high = min(j_high, self.extended_width // 2)

                assert j_low >= 0 and j_high >= 0

                # get_p find min and max p to iterate on
                # (this function is toooo slow)
                # get_p return min and max angles and a sign (top part
                # or bottom part), if the window of convolution collide
                # an axis (x or y axis), then angle computation become
                # harder, so this prototype does not do the computation
                p1, p2, j_sign = get_p(rect, self.angles, self.n_angles)

                s = 0  # sum, at the end = H[iu, iv]

                # iterate to find (U, V) in the window of convolution :
                # Angles should be in [0, PI] with this implem
                assert self.angles[self.n_angles - 1] < np.pi
                p_range = None
                if not p1 == None:
                    p_range = range(p1, p2)
                else:
                    p_range = range(self.n_angles)

                for p in p_range:
                    if not j_sign == -1:
                        for j in range(j_low, j_high + 1):  # 1st half of H
                            jcos = j * self.cos[p]
                            jsin = j * self.sin[p]
                            U = self.dtype(jcos + self.extended_width // 2)
                            V = self.dtype(jsin + self.extended_width // 2)
                            if is_in_rect((jcos, jsin), rect):
                                # Add contribution of (U, V) to (iu, iv)
                                c_data_1 = self.sino[p, j]  # filtered sinogram datas
                                x = int(round(abs(U - iu) * self.TBLSPCG))
                                y = int(round(abs(V - iv) * self.TBLSPCG))
                                convolv_coef = self.w_table[x] * self.w_table[y]
                                s += convolv_coef * c_data_1
                                nb_contribution += 1

                    if not j_sign == 1:
                        for j in range(max(j_low, 1), j_high + 1):  # 2nd half of H
                            jcos = -j * self.cos[p]
                            jsin = -j * self.sin[p]
                            U = self.dtype(jcos + self.extended_width // 2)
                            V = self.dtype(jsin + self.extended_width // 2)
                            if is_in_rect((jcos, jsin), rect):
                                # Add contribution of (U, V) to (iu, iv)
                                c_data_2 = self.sino[p, self.extended_width - j]
                                x = int(round(abs(U - iu) * self.TBLSPCG))  ###
                                y = int(round(abs(V - iv) * self.TBLSPCG))  ###
                                convolv_coef = self.w_table[x] * self.w_table[y]
                                s += convolv_coef * c_data_2
                                nb_contribution += 1

                H[iu, iv] = s
        logging.debug(
            "There was " + str(nb_contribution) + " contribution in the gridding."
        )
        return H

    ###
    ### Option 1) --> ok...
    ###
    ### Same as usual implementation in Python, but we need atomic
    ### operation to avoid writing conflicts on H matrix.
    ### Fast, but we can avoid atomic
    ### operation & store coefficient (cf option 1))
    def _pswf_convolution_gridding_opencl_atomic(self):
        """
        Same as _pswf_convolution_gridding but with OpenCL backend
        """
        self._compute_filtered_sinogram()  # self.sino contains the filtered sinogram

        add_dc = 1 if self.add_dc_freq else 0

        assert self.dtype == np.float32  # only in float32 for now
        assert self.sino.dtype == self.complex_dtype
        assert self.U_table.dtype == self.dtype
        assert self.V_table.dtype == self.dtype
        assert self.w_table.dtype == self.dtype

        # Init H
        # atomic_add is on float, so we divide H into real and imag part
        Hr = np.zeros((self.extended_width, self.extended_width), dtype=self.dtype)
        Hi = np.zeros((self.extended_width, self.extended_width), dtype=self.dtype)

        ctx = None
        first_device = True
        if first_device:
            platform = cl.get_platforms()[0]  # Select the first platform [0]
            device = platform.get_devices()[
                0
            ]  # Select the first device on this platform [0]
            ctx = cl.Context([device])  # Create a context with your device
        else:
            ctx = cl.create_some_context()

        queue = cl.CommandQueue(
            ctx, properties=cl.command_queue_properties.PROFILING_ENABLE
        )

        # Create device arrays
        d_sino = cl.Buffer(ctx, cl.mem_flags.READ_WRITE, self.sino.nbytes)
        d_U = cl.Buffer(ctx, cl.mem_flags.READ_WRITE, self.U_table.nbytes)
        d_V = cl.Buffer(ctx, cl.mem_flags.READ_WRITE, self.V_table.nbytes)
        d_w = cl.Buffer(ctx, cl.mem_flags.READ_WRITE, self.w_table.nbytes)
        d_Hr = cl.Buffer(ctx, cl.mem_flags.READ_WRITE, Hr.nbytes)  # real
        d_Hi = cl.Buffer(ctx, cl.mem_flags.READ_WRITE, Hi.nbytes)  # imag

        # Copy on the GPU
        cl.enqueue_copy(queue, d_sino, self.sino)
        cl.enqueue_copy(queue, d_U, self.U_table)
        cl.enqueue_copy(queue, d_V, self.V_table)
        cl.enqueue_copy(queue, d_w, self.w_table)
        cl.enqueue_copy(queue, d_Hr, Hr)
        cl.enqueue_copy(queue, d_Hi, Hi)

        # Build and execute the kernel
        kernel_file = os.path.join(
            "gridrec/opencl", "gridding_atomic.cl"  # TODO change this absolute path
        )
        kernel_src = open(kernel_file).read()
        program = cl.Program(ctx, kernel_src).build()
        gridsize = (np.int32(self.extended_width) // 2, np.int32(self.n_angles))
        groupsize = None

        event = program.gpu_gridding_atomic(
            queue,
            gridsize,
            groupsize,
            d_sino,
            np.int32(self.n_angles),
            np.int32(self.extended_width),
            d_U,
            d_V,
            d_w,
            d_Hr,
            d_Hi,
            np.float32(self.c_f_len),
            np.float32(self.TBLSPCG),
            np.int32(add_dc),
        )
        event.wait()
        elapsed_time = 1e-9 * (event.profile.end - event.profile.start)
        print(
            "Execution time for gridding OpenCL : %g s" % elapsed_time
        )  # warning, only computing time

        # Copy results
        cl.enqueue_copy(queue, Hr, d_Hr)
        cl.enqueue_copy(queue, Hi, d_Hi)

        # build back H
        H = Hr + 1j * Hi
        assert H.dtype == self.complex_dtype

        return H

    ###
    ### Main reconstruction function OpenCL
    ###
    def reconstruct_opencl(self, input_sino):
        """
        Construct the reconstruction with the
        gridrec algorithm (Gridding Reconstruction method)
        This function is faster than reconstruct & reconstruct_c because it is
        accelerated by OpenCL (althrough you need pyopencl to run OpenCL code,
        and a GPU to go faster (it could also run on CPU, but it does not go much faster))

        Parameters
        ------------
        input_sino: sinogram of size previously given in the reconstructor initialization
            sinogram dtype needs to be float32

        Returns
        ------------
        reconstruction with the size given in the reconstructor initialization
            type : np.float32
        """

        if not __have_opencl_gridrec__:
            raise RuntimeError(
                "reconstruct_opencl is not available, you may not have pyopencl installed"
            )

        logging.info("*** Python GRIDREC reconstruction OpenCL ***")
        # See reconstruct to get comments

        logging.debug("  [Phase 1]")
        self._check_sino(input_sino)
        logging.debug("    [Pad sinogram]")
        self.sino = self._padded_sinogram(input_sino)
        logging.debug("    [FFT-1D]")
        self.sino = self._fft_sinogram(self.sino.astype(self.complex_dtype))
        logging.debug("    [PSWF gridding] Option n°" + str(self.opencl_option))

        ### 3 Options : ###
        if self.opencl_option == 1:
            # Option 1) Usual method with atomic operation :
            self.fourier_transformed_image = (
                self._pswf_convolution_gridding_opencl_atomic()
            )
        elif self.opencl_option == 2:
            # Option 2) Found areas and each (U, V) delimited with min/max radius & angles :
            self.fourier_transformed_image = (
                self._pswf_convolution_gridding_proto_opencl()
            )
        else:  # default
            # Option 3) Init plans with original method (histogramme + contrib table) :
            self.fourier_transformed_image = self._pswf_convolution_gridding_opencl()

        logging.debug("  [Phase 2]")
        logging.debug("    [iFFT-2D]")
        self.image = self._ifft2_image(self.fourier_transformed_image)

        logging.debug("  [Phase 3]")
        logging.debug("    [FFT shift & crop]")
        self.image = self._fft_shift(self.image)
        self.cropped_image = self.image[
            self.pad_x : (self.pad_x + self.n_x), self.pad_y : (self.pad_y + self.n_y)
        ]

        logging.debug("    [Apply corrections]")
        self.recon = self._apply_corrections()
        self.recon = self._rotation_90(self.recon)

        logging.debug("*** Python GRIDREC end reconstruction OpenCL ***")

        return self.recon.astype(np.float32)
