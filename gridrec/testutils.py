# coding: utf-8
# ############################################################################
# Copyright (C) 2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# ############################################################################

__authors__ = ["Léon Roussel"]
__license__ = "MIT"
__date__ = "08/07/2022"

import numpy as np
from silx.resources import ExternalResources

utilstest = ExternalResources(
    project="nabu",
    url_base="http://www.silx.org/pub/nabu/data/",
    env_key="NABU_DATA",
    timeout=60,
)


def get_data(relative_path):
    """
    Get the numpy array strored at http://www.silx.org/pub/nabu/data/<relative_path>
    Here, relative_path should begin with 'gridrec/'
    """
    dataset_downloaded_path = utilstest.getfile(relative_path)
    return np.load(dataset_downloaded_path)


def image_distance(img1, img2, method):
    """
    Give the distance between img1 and img2

    Arguments
    ---------
    img1 : 2D numpy array
    img2 : 2D numpy array
    method : string
        Value in {"MAD", "MSE", "MAX"}
        MAD : median
        MSE : mean square error
        MAX : absolute maximum
    """
    assert np.shape(img1) == np.shape(img2)
    diff = img1 - img2
    if method == "MAD":
        return np.median(np.absolute(diff - np.median(diff)))  # todo diff abs ??
    elif method == "MSE":
        return np.square(diff).mean()
    elif method == "MAX":
        return np.max(np.abs(diff))
    else:
        raise ValueError("norm parameter is not available")


def save_array(filename, array):
    """
    Save an array in a .npy file
    """
    assert len(filename) >= 4
    assert filename[len(filename) - 4 :] == ".npy"
    np.save(filename, array)
