# coding: utf-8
# ############################################################################
# Copyright (C) 2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# ############################################################################

__authors__ = ["Léon Roussel"]
__license__ = "MIT"
__date__ = "08/07/2022"

import numpy as np
from silx.image.tomography import compute_ramlak_filter
import logging
from math import sqrt, pi, atan2


import os
from sys import executable as sys_executable
from socket import gethostname
from tempfile import gettempdir
from pathlib import Path

try:
    from pyfftw import (
        import_wisdom as pyfftw_import_wisdom,
        export_wisdom as pyfftw_export_wisdom,
        empty_aligned as pyfftw_empty_aligned,
        FFTW as pyfftw_FFTW,
    )

    __have_pyfftw__ = True
except ImportError:
    __have_pyfftw__ = False  # stil possible to use default fft with numpy


# ------------------ System ---------------------
def get_wisdom_metadata():
    """
    Get metadata on the current platform.
    FFTW wisdom works with varying performance depending on whether the plans are re-used
    on the same machine/architecture/etc.
    For more information: https://www.fftw.org/fftw3_doc/Caveats-in-Using-Wisdom.html
    """
    return {
        # "venv"
        "executable": sys_executable,
        # encapsulates sys.platform, platform.machine(), platform.architecture(), platform.libc_ver(), ...
        "hostname": gethostname(),
        "available_threads": len(os.sched_getaffinity(0)),
    }


def export_wisdom(fname, on_existing="overwrite"):
    """
    Export the current FFTW wisdom to a file.

    Parameters
    ----------
    fname: str
        Path to the file where the wisdom is to be exported
    on_existing: str, optional
        What do do when the target file already exists.
        Possible options are:
           - raise: raise an error and exit
           - overwrite: overwrite the file with the current wisdom
           - append: Import the already existing wisdom, and dump the newly combined wisdom to this file
    """
    if os.path.isfile(fname):
        if on_existing == "raise":
            raise ValueError("File already exists: %s" % fname)
        if on_existing == "append":
            import_wisdom(fname, on_mismatch="ignore")  # ?
    current_wisdom = pyfftw_export_wisdom()
    res = get_wisdom_metadata()
    for i, w in enumerate(current_wisdom):
        res[str(i)] = np.array(w)
    np.savez_compressed(fname, **res)


def import_wisdom(fname, match=["hostname"], on_mismatch="warn"):
    """
    Import FFTW wisdom for a .npz file.

    Parameters
    -----------
    fname: str
        Path to the .npz file containing FFTW wisdom
    match: list of str, optional
        List of elements that must match when importing wisdom.
        If match=["hostname"] (default), this class will only load wisdom that was saved
        on the current machine, and discard everything else.
        If match=["hostname", "executable"], wisdom will only be loaded if the file was
        created on the same machine and by the same python executable.
    on_mismatch: str, optional
        What to do when the file wisdom does not match the current platform.
        Available options:
          - "raise": raise an error (crash)
          - "warn": print a warning, don't crash
          - "ignore": do nothing
    """

    def handle_mismatch(item, loaded_value, current_value):
        msg = (
            "Platform configuration mismatch: %s: currently have '%s', loaded '%s'"
            % (item, current_value, loaded_value)
        )
        if on_mismatch == "raise":
            raise ValueError(msg)
        if on_mismatch == "warn":
            print(msg)

    wis_metadata = get_wisdom_metadata()

    try:
        loaded_wisdom = np.load(fname)
    except FileNotFoundError:
        logging.debug("    Pyfftw : no wisdom file found, init from scratch...")
        return
    for metadata_name in match:
        if metadata_name not in wis_metadata:
            raise ValueError(
                "Cannot match metadata '%s'. Available are: %s"
                % (match, str(wis_metadata.keys()))
            )
        if loaded_wisdom[metadata_name] != wis_metadata[metadata_name]:
            handle_mismatch(
                metadata_name, loaded_wisdom[metadata_name], wis_metadata[metadata_name]
            )
            return
    w = tuple(
        loaded_wisdom[k][()]
        for k in loaded_wisdom.keys()
        if k not in wis_metadata.keys()
    )
    pyfftw_import_wisdom(w)


def get_wisdom_file(
    directory=None,
    name_template="fftw_wisdom_{whoami}_{hostname}.npz",
    create_dirs=True,
):
    """
    Get a file path for storing FFTW wisdom.

    Parameters
    ----------
    directory: str, optional
        Directory where the file is created. By default, files are written in a temporary directory.
    name_template: str, optional
        File name pattern. The following patterns can be used:
           - {whoami}: current username
           - {hostname}: machine name
    create_dirs: bool, optional
        Whether to create (possibly nested) directories if needed.
    """
    directory = directory or gettempdir()

    try:
        whoami = os.getlogin()
    except:
        whoami = "defaultUser"

    file_basename = name_template.format(whoami=whoami, hostname=gethostname())
    out_file = os.path.join(directory, file_basename)
    if create_dirs:
        Path(os.path.dirname(out_file)).mkdir(parents=True, exist_ok=True)
    return out_file


# ------------------ Filter ---------------------
def filter_ramlak_old(length):
    """
    Approximation of ramlak filter
    """
    return np.abs(np.arange(0, length, 1) / length)


def filter_ramlak_dc_old(length):
    """
    Ramlak filter cumputed with dtype=np.float32
    """
    return np.fft.fft(compute_ramlak_filter(length)).real


# ------------------ Math ---------------------
def legendre(i, x):
    """
    Calculate L_i(x) where i is the degree
    of the Legendre polynomial
    """
    # Calculation with the following recurrence formula :
    # P0(x) = 1, P1(x) = x [1]
    # (n+1) Pn+1(x) = (2n+1)xPn(x) - nPn-1(x)
    # <=> Pn(x) = ((2n-1)/n) * x * Pn-1(x) - ((n-1)/n) * Pn-2(x) [2]
    # This last formula is used in the following loop,
    # last is Pn-1(x) & penult (penultimate) is Pn-2(x)
    assert x >= -1 and x <= 1
    penult = 1.0
    last = x
    cur = 0
    mxlast = 0
    if i == 0:  # [1]
        return 1
    if i == 1:  # [1]
        return x
    for j in range(2, int(i + 1)):  # [2]
        mxlast = -(x * last)  # minus x last
        cur = -(2 * mxlast + penult) + (penult + mxlast) / j
        penult = last
        last = cur
    return cur

    # Old legendre sum computation :
    # (compute sum while computing the legendre
    # polynomials by recurrence)
    # It has better performances, but it is less
    # readable.
    """
    def _compute_sum_legendre(self, degree, x):
        assert x >= -1 and x <= 1
        penult = 1.0
        last = x
        cur = 0
        mxlast = 0
        res_sum = self.LEGENDRE_COEFS[0]  # = P0(x) * self.LEGENDRE_COEFS[0]

        # Calculation with the following recurrence formula :
        # P0(x) = 1, P1(x) = x
        # (n+1) Pn+1(x) = (2n+1)xPn(x) - nPn-1(x)
        # <=> Pn(x) = ((2n-1)/n) * x * Pn-1(x) - ((n-1)/n) * Pn-2(x)
        # This last formula is used in the following loop,
        # last is Pn-1(x) & penult (penultimate) is Pn-2(x)

        # P0 and P1 are already known, the loop start at 2:
        for j in range(2, int(degree + 1)):
            mxlast = -(x * last)  # minus x last
            cur = -(2 * mxlast + penult) + (penult + mxlast) / j
            if j % 2 == 0:
                res_sum += cur * self.LEGENDRE_COEFS[j >> 1]
            penult = last
            last = cur

        return res_sum
    """


# ------------------ Getting angles for OpenCL implem ---------------------
# This was used to prototype, but solution is not efficient.
# But functions remain here...


def min_dist_to_rect(rect, p):
    """
    Give min distance between rect = ((minx, miny), (maxx, maxy))
    and point p = (px, py)
    """
    dx = max(
        rect[0][0] - p[0], 0, p[0] - rect[1][0]
    )  # max(rect.min.x - p.x, 0, p.x - rect.max.x)
    dy = max(
        rect[0][1] - p[1], 0, p[1] - rect[1][1]
    )  # max(rect.min.y - p.y, 0, p.y - rect.max.y)
    return sqrt(dx**2 + dy**2)


def max_dist_to_rect(rect, p):
    """
    Give max distance between rect = ((minx, miny), (maxx, maxy))
    and point p = (px, py)
    """
    dx = max(
        abs(rect[1][0] - p[0]), abs(p[0] - rect[0][0])
    )  # max(|rect.max.x - p.x|, |p.x - rect.min.x|)
    dy = max(
        abs(rect[1][1] - p[1]), abs(p[1] - rect[0][1])
    )  # max(|rect.max.y - p.y|, |p.y - rect.min.y|)
    return sqrt(dx**2 + dy**2)


def is_in_rect(p, rect):
    """
    Return if point p = (px, py) is in rectangle rect = ((minx, miny), (maxx, maxy))
    """
    is_ax_x = rect[0][0] <= p[0] and p[0] <= rect[1][0]
    is_ax_y = rect[0][1] <= p[1] and p[1] <= rect[1][1]
    return is_ax_x and is_ax_y


def get_extreme_points(rect):
    """
    Return extremal points that construct the "cone" TODO
    """

    def sign(x):
        return 1 if x >= 0 else -1

    def nearest_zero(x, y):
        if abs(x) < abs(y):
            return x
        else:
            return y

    def farest_zero(x, y):
        if abs(x) < abs(y):
            return y
        else:
            return x

    o = (0, 0)

    if is_in_rect(o, rect):  # different cosine & sine
        return (None, None)  # no extreme points

    if not sign(rect[0][0]) == sign(rect[1][0]):  # different sign of cosine
        return (None, None)  # implemented but not taking into account
        """ord = None
        if rect[0][1] >= 0:  # both sin should be positive
            assert rect[1][1] >= 0
            ord = min(rect[0][1], rect[1][1])
        else:  # both sign should be negative
            assert rect[0][1] <= 0 and rect[1][1] <= 0
            ord = max(rect[0][1], rect[1][1])"""

        return ((rect[0][0], ord), (rect[1][0], ord))

    if not sign(rect[0][1]) == sign(rect[1][1]):  # different sign of sine
        return (None, None)  # implemented but not taking into account
        """ ord = None
        if rect[0][0] >= 0:  # both cos should be positive
            assert rect[1][0] >= 0
            ord = min(rect[0][0], rect[1][0])
        else:  # both sign should be negative
            assert rect[0][0] <= 0 and rect[1][0] <= 0
            ord = max(rect[0][0], rect[1][0])

        return ((ord, rect[0][1]), (ord, rect[1][1]))"""

    # sine & cosine same sign :
    return (
        (
            nearest_zero(rect[0][0], rect[1][0]),
            farest_zero(rect[0][1], rect[1][1]),
        ),
        (
            farest_zero(rect[0][0], rect[1][0]),
            nearest_zero(rect[0][1], rect[1][1]),
        ),
    )


def clock_order(p1, p2):
    cross_p = np.cross(np.append(p1, [0]), np.append(p2, [0]))
    if cross_p[2] < 0:  # z axes
        return (p2, p1)
    return (p1, p2)


def get_angle(p):
    """
    Angle in 0, 2.pi
    """
    a = atan2(p[1], p[0])
    if p[1] < 0:  # left part
        a = a + 2 * pi
    return a


def get_precise_p(theta1, theta2, angles, n_angles):
    """
    Window should be entirely in the top or bottom half !

    Return (p_start, p_end, j_sign)
    """
    assert (
        0 <= theta1 <= pi
        and 0 <= theta2 <= pi
        or pi <= theta1 <= 2 * pi
        and pi <= theta2 <= 2 * pi
    )
    j_sign = 1 if 0 <= theta1 <= pi else -1
    if theta1 >= pi:
        theta1 -= pi
        theta2 -= pi
    p_start = np.searchsorted(angles, theta1)
    p_end = min(np.searchsorted(angles, theta2), n_angles)  # range(p_start, p_end)
    # print(theta1)

    return p_start, p_end, j_sign


def get_p(rect, angles, n_angles):
    """
    Get the minimum and maximum p (index to access in angles array)
    to search when looking at (iu, iv) which is at the center of rect

    If rect collide an axis (x or y), then None is returned
    (computation is harder and not required for this prototype)
    """
    # if possible give p else None --> every p
    point_1, point_2 = get_extreme_points(rect)

    if point_1 == None:  # rect intersect 1 or 2 axis
        return (None, None, None)
    else:
        point_1, point_2 = clock_order(point_1, point_2)
        theta1, theta2 = get_angle(point_1), get_angle(point_2)

        # print(theta1, theta2)
        return get_precise_p(theta1, theta2, angles, n_angles)
