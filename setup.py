# coding: utf-8
# ############################################################################
# Copyright (C) 2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# ############################################################################
import setuptools
from os import environ
from glob import glob

from gridrec import version


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

# Mechanism for optional compilation
def boolify(str_):
    res = True if str_.lower() in ["1", "true", "yes"] else False
    return res


do_compilation = boolify(environ.get("DO_COMPILE", "true"))
if do_compilation:
    try:
        from pybind11 import get_cmake_dir

        # Available at setup time due to pyproject.toml
        from pybind11.setup_helpers import Pybind11Extension, build_ext

        __have_pybind11__ = True
    except ImportError:
        __have_pybind11__ = False

# ---------------

ext_modules = None
if do_compilation and __have_pybind11__:
    ext_modules = [
        Pybind11Extension(
            "gridrec.pswf_gridding_c",
            sorted(
                glob("gridrec/src/*.cpp"),
            ),
            # Example: passing in the version to the compiled code
            define_macros=[("VERSION_INFO", version)],
        )
    ]

# ------------

setuptools.setup(
    name="gridrec",
    version=version,
    author="Léon Roussel",
    author_email="leon.roussel@esrf.fr",
    description="Test package for gridrec",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.esrf.fr/lroussel/gridrec",
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    packages=setuptools.find_packages(),
    package_data={
        "gridrec": [
            "src/*.c",
            "src/*.cpp",
            "src/*.h",
        ],
    },
    include_package_data=True,
    ext_modules=ext_modules,
    install_requires=["numpy", "silx", "pytest"],
    python_requires=">=3.6",
)
