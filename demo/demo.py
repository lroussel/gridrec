# coding: utf-8
# /*##########################################################################
# Copyright (C) 2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ############################################################################*/

__authors__ = ["Léon Roussel"]
__license__ = "MIT"
__date__ = "08/07/2022"

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from gridrec.testutils import get_data
from gridrec.reconstruction import GridrecReconstructor

import cProfile  # profiling


def load_demo(show_result=True, reconstruction_c=True):
    # input sinogram should be in the directory "http://www.silx.org/pub/nabu/data/"
    # values of the sinogram should be float coded on 32 bits
    # (gridrec reconstruction output values are by default on 32bits)
    # input filename is <name>_sino_<n_angles>_<d_width>.dat
    name = "brain"  # name should be either 'sl' or 'brain'
    filter_name = "hamming"
    dwidth = 768  # detector width
    n_angles = 500  # number of acquisition angles

    input_name = (
        "gridrec/" + name + "_sino_" + str(n_angles) + "_" + str(dwidth) + ".npy"
    )

    # get_data is a function of the module gridrec (gridrec/testutils.py)
    # it downloaded data on a server
    sino = get_data(input_name).astype(np.float64)

    # -------------------- Init class : -----------------------
    options = {
        "add_dc_freq": True,  # new case to take into account the dc frequency
        "python_accelerate": True,  # just a fastest implem in python
    }

    # There are 2 steps :
    #   - initialization : create the object GridrecReconstructor
    #   - reconstruction : call the function gridrec_reconstruction on the object

    # ------------- Init --------------
    reconstructor = GridrecReconstructor(
        np.shape(sino),
        slice_shape=(dwidth, dwidth),
        filter_name=filter_name,
        backend_fft="fftw",
        options=options,
    )

    # ------------- Compute the reconstruction : --------------
    # Two choices : full python or a mix of c and python (faster)
    reconstruction = None
    if reconstruction_c:
        reconstruction = reconstructor.reconstruct_c(sino)
    else:
        reconstruction = reconstructor.reconstruct(sino)

    # ---------------   Display results   ---------------------
    if show_result:
        # Display resulting reconstruction :
        plt.title("Reconstructed image")
        plt.imshow(reconstruction)
        plt.colorbar()
        plt.show()

        # Display resulting reconstruction with rectangle on targeted slice :
        n_line, n_column = np.shape(reconstruction)
        targeted_slice = n_line // 2
        fig, ax = plt.subplots(nrows=1, ncols=1)
        fig.suptitle("Targeted slice of the image :")
        rect = patches.Rectangle(
            (0, targeted_slice - 4),
            n_column,
            9,
            edgecolor="r",
            facecolor="none",
            angle=0,
        )
        ax.imshow(reconstruction)
        ax.add_patch(rect)
        plt.show()

        # Display the targeted slice of the reconstructed image :
        plt.title("Targeted slice of the reconstructed image")

        original_brain = np.pad(
            get_data("gridrec/brain_phantom.npz")["data"],
            pad_width=[(128, 128), (128, 128)],
        )  # it is 512, pad to go to 768
        plt.plot(original_brain[targeted_slice], label="original image")
        plt.plot(reconstruction[targeted_slice], label="reconstruction")
        plt.legend()
        plt.grid()
        plt.show()


if __name__ == "__main__":
    # cProfile.run(
    #    "load_demo(show_result=False, reconstruction_c=True)", filename="./profile.prof"
    # ) # uncomment this to profile
    load_demo(show_result=True, reconstruction_c=True)
