# coding: utf-8
# /*##########################################################################
# Copyright (C) 2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ############################################################################*/

__authors__ = ["Léon Roussel"]
__license__ = "MIT"
__date__ = "08/07/2022"

import numpy as np
import matplotlib.pyplot as plt
from gridrec.testutils import get_data
from gridrec.reconstruction import GridrecReconstructor

# (see demo.py to understand the code)
# See curve and patch it


def load_demo(show_result=True):
    name = "brain"
    filter_name = "hamming"
    dwidth = 768
    n_angles = 500

    input_name = (
        "gridrec/" + name + "_sino_" + str(n_angles) + "_" + str(dwidth) + ".npy"
    )

    sino = get_data(input_name).astype(np.float64)

    # -------------------- Init class : -----------------------

    options = {
        "add_dc_freq": True,
    }
    r_curve = GridrecReconstructor(
        np.shape(sino),
        slice_shape=(dwidth, dwidth),
        filter_name=filter_name,
        options=options,
    )

    options["add_dc_freq"] = True
    options["pswf_algebra"] = True  # recompute legendre coefficients
    options["C"] = 7.0  # choose C
    options[
        "continuous_convo"
    ] = 2  # continuous size of filter of convolution + correct mapping of TBLSPCG
    r_uncurve = GridrecReconstructor(
        np.shape(sino),
        slice_shape=(dwidth, dwidth),
        filter_name=filter_name,
        options=options,
    )

    recon_curve = r_curve.reconstruct_c(sino)
    recon_uncurve = r_uncurve.reconstruct_c(sino)

    # ---------------   Display results   ---------------------
    if show_result:
        # Display resulting reconstruction :
        plt.title("Difference between reconstructions")
        plt.imshow(recon_uncurve - recon_curve)
        plt.colorbar()
        plt.show()

        # Display resulting reconstruction with rectangle on targeted slice :
        n_line = dwidth
        targeted_slice = n_line // 2

        plt.title("Curve, random normalization")

        original_brain = np.pad(
            get_data("gridrec/brain_phantom.npz")["data"],
            pad_width=[(128, 128), (128, 128)],
        )  # it is 512, pad to go to 768

        plt.plot(original_brain[targeted_slice], label="ground truth")
        plt.plot(recon_uncurve[targeted_slice], label="with curve correction")
        plt.plot(recon_curve[targeted_slice], label="without curve correction")
        plt.legend()
        plt.grid()
        plt.show()


def load_demo2(show_result=True):
    name = "brain"
    filter_name = "hamming"
    dwidth = 768
    n_angles = 500

    input_name = (
        "gridrec/" + name + "_sino_" + str(n_angles) + "_" + str(dwidth) + ".npy"
    )

    sino = get_data(input_name).astype(np.float64)

    # -------------------- Init class : -----------------------

    options = {
        "add_dc_freq": True,
    }

    options["pswf_algebra"] = True  # recompute legendre coefficients
    options[
        "continuous_convo"
    ] = 2  # continuous size of filter of convolution + correct mapping of TBLSPCG
    # you can test with this parameter to 0 to see the curve

    C_list = np.linspace(6.3, 7, 5)  # different value of C in [6.3, 7]

    n_line = dwidth
    targeted_slice = n_line // 2

    plt.title("No curve, different C")

    for c in C_list:
        options["C"] = c
        print(options)
        r = GridrecReconstructor(
            np.shape(sino),
            slice_shape=(dwidth, dwidth),
            filter_name=filter_name,
            options=options,
        )
        recon = r.reconstruct_c(sino)
        plt.plot(recon[targeted_slice], label="C=" + str(round(c, 2)))

    plt.legend()
    plt.grid()
    plt.show()


if __name__ == "__main__":
    load_demo(show_result=True)
    load_demo2(show_result=True)
