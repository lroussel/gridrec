# coding: utf-8
# /*##########################################################################
# Copyright (C) 2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ############################################################################*/

__authors__ = ["Léon Roussel"]
__license__ = "MIT"
__date__ = "08/07/2022"

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from gridrec.testutils import get_data
from gridrec.reconstruction import GridrecReconstructor
import time

import cProfile  # profiling


def load_demo(show_result=True):
    # input sinogram should be in the directory "http://www.silx.org/pub/nabu/data/"
    # values of the sinogram should be float coded on 32 bits
    # (gridrec reconstruction output values are by default on 32bits)
    # input filename is <name>_sino_<n_angles>_<d_width>.dat
    name = "brain"  # name should be either 'sl' (shepp-logan) or 'brain'
    filter_name = "ramlak"  # "hamming"
    dwidth = 768  # detector width
    n_angles = 500  # number of acquisition angles

    input_name = (
        "gridrec/" + name + "_sino_" + str(n_angles) + "_" + str(dwidth) + ".npy"
    )

    sino = get_data(input_name).astype(np.float32)

    options = {
        "add_dc_freq": True,  # new case to take into account the dc frequency
        "python_accelerate": True,  # just a fastest implem in python
        "opencl_init": True,
    }

    reconstructor = GridrecReconstructor(
        np.shape(sino),
        slice_shape=(dwidth, dwidth),
        filter_name=filter_name,
        backend_fft="numpy",
        pad_ratio=1.33,
        options=options,
        dtype=np.float32,  # OpenCL implem only available with float32
    )

    # ------------- Compute the reconstruction : --------------

    print("Start of OpenCL reconstruction")
    t1 = time.time()
    reconstruction = reconstructor.reconstruct_opencl(sino)
    t2 = time.time()
    print("OpenCL reconstruction took : ", t2 - t1)

    print("Start of C reconstruction")
    t3 = time.time()
    reconstruction_c = reconstructor.reconstruct_c(sino)
    t4 = time.time()
    print("C reconstruction took : ", t4 - t3)
    base_recon = get_data("gridrec/recon_brain_dc_py.npy")  # only for brain image

    # ---------------   Display results   ---------------------
    if show_result:
        plt.imshow(reconstruction - reconstruction_c)
        plt.title("Difference between C & OpenCL reconstruction")
        plt.colorbar()
        plt.show()

        # Display resulting reconstruction :
        plt.title("Reconstructed image")
        plt.imshow(reconstruction)
        plt.colorbar()
        plt.show()

        # Display resulting reconstruction with rectangle on targeted slice :
        n_line, n_column = np.shape(reconstruction)
        targeted_slice = n_line // 2
        fig, ax = plt.subplots(nrows=1, ncols=1)
        fig.suptitle("Targeted slice of the image :")
        rect = patches.Rectangle(
            (0, targeted_slice - 4),
            n_column,
            9,
            edgecolor="r",
            facecolor="none",
            angle=0,
        )
        ax.imshow(reconstruction)
        ax.add_patch(rect)
        plt.show()

        # Display the targeted slice of the reconstructed image :
        plt.title("Targeted slice of the reconstructed image")
        plt.plot(reconstruction[targeted_slice], label="reconstruction")
        plt.legend()
        plt.grid()
        plt.show()

        # Display difference # only for brain image
        plt.imshow(reconstruction - base_recon)
        plt.title("Difference between original python & OpenCL reconstruction")
        plt.colorbar()
        plt.show()


def load_demo_bamboo(show_result=True):
    name = "bamboo"  # name should be either 'sl' (shepp-logan) or 'brain'
    filter_name = "ramlak"  # "hamming"
    dwidth = 4124  # detector width
    n_angles = 2200  # number of acquisition angles

    input_name = (
        "gridrec/" + name + "_sino_" + str(n_angles) + "_" + str(dwidth) + ".npy"
    )

    sino = get_data(input_name).astype(np.float32)

    options_1 = {
        "add_dc_freq": True,  # new case to take into account the dc frequency
        "python_accelerate": True,  # just a fastest implem in python
        "opencl_init": True,
        "opencl_option": 1,
    }

    options_2 = {
        "add_dc_freq": True,  # new case to take into account the dc frequency
        "python_accelerate": True,  # just a fastest implem in python
        "opencl_init": True,
        "opencl_option": 3,
    }

    reconstructor_atomic = GridrecReconstructor(
        np.shape(sino),
        slice_shape=(dwidth, dwidth),
        filter_name=filter_name,
        backend_fft="numpy",
        pad_ratio=1.33,
        options=options_1,
        dtype=np.float32,  # OpenCL implem only available with float32
    )

    reconstructor_plan = GridrecReconstructor(
        np.shape(sino),
        slice_shape=(dwidth, dwidth),
        filter_name=filter_name,
        backend_fft="numpy",
        pad_ratio=1.33,
        options=options_2,
        dtype=np.float32,  # OpenCL implem only available with float32
    )

    # ------------- Compute the reconstruction : --------------

    print("Start of OpenCL reconstruction atomic")
    t1 = time.time()
    reconstruction_atomic = reconstructor_atomic.reconstruct_opencl(sino)
    t2 = time.time()  # gridding reconstruction : 0.6s
    print("OpenCL reconstruction atomic add took : ", t2 - t1)

    print("Start of OpenCL reconstruction plan")
    t1 = time.time()
    reconstruction_plan = reconstructor_plan.reconstruct_opencl(sino)
    t2 = time.time()  # gridding reconstruction : 0.2s
    print("OpenCL reconstruction plan add took : ", t2 - t1)

    print("Start of C reconstruction")
    t3 = time.time()
    reconstruction_c = reconstructor_plan.reconstruct_c(sino)
    t4 = time.time()
    print("C reconstruction took : ", t4 - t3)

    # ---------------   Display results   ---------------------
    if show_result:
        plt.imshow(reconstruction_atomic - reconstruction_plan)
        plt.title("Difference between OpenCL atomic & OpenCL plan")
        plt.colorbar()
        plt.show()

        plt.imshow(reconstruction_c - reconstruction_atomic)
        plt.title("Difference between C & OpenCL atomic")
        plt.colorbar()
        plt.show()

        plt.imshow(reconstruction_c - reconstruction_plan)
        plt.title("Difference between C & OpenCL plan")
        plt.colorbar()
        plt.show()


if __name__ == "__main__":
    # Uncomment lines below to test different functions

    # Demo profiled :
    # cProfile.run("load_demo(show_result=True)", filename="./profile.prof")
    # to launch the profile, install 'snakeviz' and then 'snakeviz profile.prof'

    # Demo brain :
    load_demo(show_result=True)

    # Demo bamboo :
    # load_demo_bamboo(show_result=True)
