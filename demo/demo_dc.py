# coding: utf-8
# /*##########################################################################
# Copyright (C) 2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ############################################################################*/

__authors__ = ["Léon Roussel"]
__license__ = "MIT"
__date__ = "08/07/2022"

import numpy as np
import matplotlib.pyplot as plt
from gridrec.testutils import get_data
from gridrec.reconstruction import GridrecReconstructor

# (see demo.py to understand the code)
# DC = continuous frequency, case j = 0 added


def load_demo(show_result=True):
    name = "brain"
    filter_name = "hamming"
    dwidth = 768
    n_angles = 500

    input_name = (
        "gridrec/" + name + "_sino_" + str(n_angles) + "_" + str(dwidth) + ".npy"
    )

    sino = get_data(input_name).astype(np.float64)

    # -------------------- Init class : -----------------------

    options = {"add_dc_freq": False}
    r = GridrecReconstructor(
        np.shape(sino),
        slice_shape=(dwidth, dwidth),
        filter_name=filter_name,
        options=options,
    )

    options["add_dc_freq"] = True
    r_j0 = GridrecReconstructor(
        np.shape(sino),
        slice_shape=(dwidth, dwidth),
        filter_name=filter_name,
        options=options,
    )

    recon = r.reconstruct_c(sino)
    recon_j0 = r_j0.reconstruct_c(sino)

    # ---------------   Display results   ---------------------
    if show_result:
        # Display resulting reconstruction :
        plt.title("Difference between reconstructions (with and without dc component)")
        plt.imshow(recon_j0 - recon)
        plt.colorbar()
        plt.show()

        # Display resulting reconstruction with rectangle on targeted slice :
        n_line, n_column = np.shape(recon)
        targeted_slice = n_line // 2

        plt.title("Slice of the reconstructed image")

        original_brain = np.pad(
            get_data("gridrec/brain_phantom.npz")["data"],
            pad_width=[(128, 128), (128, 128)],
        )  # it is 512, pad to go to 768

        # plt.plot(original_brain[targeted_slice], label="original image")
        plt.plot(recon[targeted_slice], label="without dc component")
        plt.plot(recon_j0[targeted_slice], label="with dc component")
        plt.legend()
        plt.grid()
        plt.show()


if __name__ == "__main__":
    load_demo(show_result=True)
