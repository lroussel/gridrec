# gridrec

Tomography is an imaging technique that aims to reconstruct volumes by using penetrating waves. The "gridrec" (gridding reconstruction) algorithm reconstruct one slice of the volume. It could be an alternative to the filtered backprojection algorithm.

## Installation 

The project is not on PyPi, but you can still install it by downloading the repository : 

```
git clone <adress_repo>
```

and then install the package :

```
pip install <path_to_the_repo_on_your_machine>
```

To use the fastest reconstruction method (C & OpenCL backend), you will need to install `pyopencl` and `pybind11`. Be sure to have `pybind11` before installing the package, otherwise the C++ files will not be compiled during the installation of the package.

## Usage

To reconstruct, first create a reconstructor object :

```
reconstructor = GridrecReconstructor((n_angles, detector_width))
```

to custom the reconstruction, you have access to a bunch of parameters and options. To see the parameter list, go into the class GridrecReconstructor, to see the options, go into the description of the _set_options() method.

Then, to reconstruct, call the reconstruction method on the input sinogram :

```
reconstruction = reconstructor.reconstruct(sinogram)
```

To accelerate the process, check the options, `reconstruct_c` or `reconstruct_opencl`.

## Demonstration

Demos are available in the directory `./demo`. Understanding those scripts would be a great way to start using this package !

Those demonstration are using images and sinograms from a storage repository, but you can import your own sinograms.

## Perspectives

- Precision : unify the implementation to get smaller gap between the different implementation 
- Script division : divide `reconstruction.py` into pieces. Currently very long and sometimes hard to locate elements (filter section, gridding section, ...)
- OpenCL implem : the advancements are describe in the script `reconstruction.py` at line around 1780. To summarize, transfer from host to device are the bottlneck currently.

## License

The license of the project is MIT. 

Principal author : Léon Roussel (leon.roussel@grenoble-inp.org)
